# Dermascopic Melanoma Image Classification in PyTorch

- [Dermascopic Melanoma Image Classification in PyTorch](#dermascopic-melanoma-image-classification-in-pytorch)
  - [About](#about)
    - [Results](#results)
    - [Trying it out](#trying-it-out)
    - [Examples](#examples)
  - [Trained Model Performance](#trained-model-performance)
    - [Model Statistics](#model-statistics)
      - [model_epoch_026.pth](#model_epoch_026pth)
      - [model_epoch_043.pth](#model_epoch_043pth)
      - ["Bayesian" Joint Model](#bayesian-joint-model)
  - [Interpretation of Model Diagnosis](#interpretation-of-model-diagnosis)
    - [Diagnostic Performance](#diagnostic-performance)
    - [Making Diagnostic Inference](#making-diagnostic-inference)
      - [Calculating Disease Odds](#calculating-disease-odds)
  - [Training the Model](#training-the-model)
    - [Example](#example)
  - [Scripts](#scripts)
    - [main.py](#mainpy)
      - [main.py Arguments](#mainpy-arguments)
      - [Resuming training](#resuming-training)
    - [runModel.py](#runmodelpy)
      - [runModel.py Arguments](#runmodelpy-arguments)
    - [testSetAccuracy.py](#testsetaccuracypy)
      - [testSetAccuracy.py Arguments](#testsetaccuracypy-arguments)
      - [testSetAccuracy.py Example](#testsetaccuracypy-example)
    - [testFinalAccuracy.py](#testfinalaccuracypy)
      - [Output:](#output)
    - [constructDataset.py](#constructdatasetpy)
      - [constructDataset.py Arguments](#constructdatasetpy-arguments)
    - [augmentDataset.py](#augmentdatasetpy)
      - [augmentDataset.py Arguments](#augmentdatasetpy-arguments)
  - [Appendix](#appendix)
    - [A1. ISIC Archive Downloader](#a1-isic-archive-downloader)
    - [A2. Preparing the ISIC Dataset for Training & Evaluation](#a2-preparing-the-isic-dataset-for-training--evaluation)
    - [A3. Intersection Over Union (IoU)](#a3-intersection-over-union-iou)
    - [A4. Further Reading](#a4-further-reading)

## About

<table>
<tr>
<td> <img src="./examples/test_benign/predicted/ISIC_0005821.jpegpredicted.jpeg" alt="predicted_benign" style="width: 250px;"/> </td>
<td> <img src="./examples/test_malignant/predicted/ISIC_6056121_predicted.jpeg" alt="mask" style="width: 250px;"/> </td>
</tr>
</table>

This repository is a continuation of my project for identifying and classifying melanocytic moles. You can check out the other repo [here](https://gitlab.com/dibz15/dermascopic-melanoma-image-segmentation-in-pytorch). It is for segmentation of melanocytic images, and was used to train the classifiers that I'm discussing here. This repository builds on that and is for mole classifiers. Overall, they're two separate tools intended to be used together.

Note that this project is for _classification_, not _detection_ and/or _masking_. If you're interested in masking/segmentation, see the project mentioned above.

This classifier project utilizes several different classifiers, including MobileNet-V2, MobileNet-V3, and EfficientNet-B1/B2. The intention is for them to be able to be run on mobile devices or low-end computers, so I opted for these rather than other larger deep learning networks such as ResNet.

### Results

My best model is a joint between two separately trained models. I discuss their differences and how I used them together more under [Trained Model Performance](#trained-model-performance).

Here is an overview of their performance on my validation and test sets. Given the high class imbalance (10% malignant) the performance is really great. 

<table>
<tr>
<td> <img src="./examples/joint/malig1st_malig026_val.png" alt="validation conf matrix model 026" style="width: 300px;"/> </td>
<td> <img src="./examples/joint/malig1st_malig026_test.png" alt="test conf matrix model 026" style="width: 300px;"/> </td>
</tr>
<tr>
<td> <img src="./examples/joint/malig1st_malig026_val_roc.png" alt="validation roc model 026" style="width: 300px;"/> </td>
<td> <img src="./examples/joint/malig1st_malig026_test_roc.png" alt="test roc model 026" style="width: 300px;"/> </td>
</tr>
<tr>
<td> <img src="./examples/joint/malig1st_malig026_val_prc.png" alt="validation prc model 026" style="width: 300px;"/> </td>
<td> <img src="./examples/joint/malig1st_malig026_test_prc.png" alt="test prc model 026" style="width: 300px;"/> </td>
</tr>
</table>

Statistics of both sets combined: 

    Accuracy: 0.94569822075
    AUROC: ~0.95
    AUPRC: ~0.83
    Specificity/TNR: 0.9615119088712462, FPR: 0.03848809112875384
    PPV: 0.718078381795196, NPV: 0.9772826945004824
    Sensitivity/TPR: 0.814336917562724, FNR: 0.18566308243727603
    Informedness: 0.77584882643397
    LR+: 21.158152916405506, LR-: 0.19309493800782215, Diag odds ratio: 109.5738352061223

Additionally, looking at the ROC curve, it appears that in order to achieve a sensitivity of 95%, we could still achieve a specificity of 75%. This is really good, and is actually better than the performance seen in state-of-the-art methods from 2020 ([“Detection of Malignant Melanoma Using Artificial Intelligence: An Observational Study of Diagnostic Accuracy” (2020)](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6936633/) ). AUPRC is also 0.83, which, given that the malignant rate in the dataset is only about 0.1, this is a great result. This is excellent performance from small, mobile models.

### Trying it out

I have in this repository two models that I trained on an RTX2060 until convergence stored in `./saved_models` To try a trained model on the image, simply choose the trained model and run the following script:

    python runModel.py --input_file <image-path> --model_file ./saved_models/<trained_model_file>

This will evaluate the input image file with its predicted result (benign or malignant, with a 'confidence').

### Examples

Here are some examples using different commandline options to change the output.

If we want to run our model on a collection of image files and get an aggregated result, all that's needed is to provide the path to the directory containing image files. 

For example, to predict the folder at `./examples/test_benign` and get an prediction output file `./examples/test_benign/predicted.txt` we can run:

    python runModel.py --input_file ./examples/test_benign --model_file ./saved_models/model_epoch_026.pth --output_file ./examples/test_benign/predicted.txt

For me the output file looks like this:

    ISIC_0002041.jpeg: Prediction - benign : 0.999
    ISIC_0005821.jpeg: Prediction - benign : 1.000
    ISIC_0008493.jpeg: Prediction - benign : 1.000
    ISIC_1649410.jpeg: Prediction - benign : 1.000
    ISIC_1796387.jpeg: Prediction - benign : 0.999
    ISIC_2532039.jpeg: Prediction - benign : 1.000
    ISIC_5110209.jpeg: Prediction - benign : 1.000
    ISIC_9784337.jpeg: Prediction - benign : 1.000

So you can see that it successfully predicts all the benign samples. A corresponding run for `./examples/test_malignant` gives me the following results:

    ISIC_0000414.jpeg: Prediction - malignant : 1.000
    ISIC_0032372.jpeg: Prediction - malignant : 1.000
    ISIC_0033304.jpeg: Prediction - malignant : 1.000
    ISIC_0057609.jpeg: Prediction - malignant : 0.985
    ISIC_3286186.jpeg: Prediction - malignant : 1.000
    ISIC_6056121.jpeg: Prediction - malignant : 0.999



## Trained Model Performance

The trained model performance on the small EfficientNet-B2 models what was I found to be best, given my limited resources. I only trained up to epoch 50, as that is where convergence seemed to be reached.

Here is a screenshot from my tensorboard training for my last two runs of the B2 models:

![Tensorboard results](./examples/tensorboard_results.png)

My two models in `./saved_models` I selected when considering their performance on both evaluation and test sets. One (`model_epoch_026.pth`) is slightly better at detecting malignant examples (lower false-negative rate) and the other (`model_epoch_043.pth`) is slightly better at detecting benign examples (lower false-positive rate). Selecting one or the other depends on the warranted trade-offs. 

### Model Statistics

#### model_epoch_026.pth

Here are the confusion matrices for this model on my validation set (9256 benign, 1118 malignant) and test set (2332 benign, 277 malignant).

Below that are the ROC curves, also corresponding to the validation and test sets.

Below that are the PRC curves.

<table>
<tr>
<td> <img src="./examples/model_epoch_026_ISICVal_conf.png" alt="validation conf matrix model 026" style="width: 300px;"/> </td>
<td> <img src="./examples/model_epoch_026_ISICTest_conf.png" alt="test conf matrix model 026" style="width: 300px;"/> </td>
</tr>
<tr>
<td> <img src="./examples/model_epoch_026_ISICVal_roc.png" alt="validation roc model 026" style="width: 300px;"/> </td>
<td> <img src="./examples/model_epoch_026_ISICTest_roc.png" alt="test roc model 026" style="width: 300px;"/> </td>
</tr>
<tr>
<td> <img src="./examples/model_epoch_026_ISICVal_prc.png" alt="validation prc model 026" style="width: 300px;"/> </td>
<td> <img src="./examples/model_epoch_026_ISICTest_prc.png" alt="test prc model 026" style="width: 300px;"/> </td>
</tr>
</table>

Statistics of both sets combined:

    Accuracy: 0.946391434954
    AUROC: ~0.95
    AUPRC: ~0.83
    Specificity/TNR: 0.9644459785985502, FPR: 0.03555402140144981
    PPV: 0.7294812869336835, NPV: 0.97521815008726
    Sensitivity/TPR: 0.796415770609319, FNR: 0.203584229390681
    Informedness: 0.7608617492078693
    LR+: 22.400160072380533, LR-: 0.21108930298668677, Diag odds ratio: 106.1169834541227


#### model_epoch_043.pth

<table>
<tr>
<td> <img src="./examples/model_epoch_043_ISICVal_conf.png" alt="validation conf matrix model 026" style="width: 300px;"/> </td>
<td> <img src="./examples/model_epoch_043_ISICTest_conf.png" alt="test conf matrix model 026" style="width: 300px;"/> </td>
</tr>
<tr>
<td> <img src="./examples/model_epoch_043_ISICVal_roc.png" alt="validation roc model 026" style="width: 300px;"/> </td>
<td> <img src="./examples/model_epoch_043_ISICTest_roc.png" alt="test roc model 026" style="width: 300px;"/> </td>
</tr>
<tr>
<td> <img src="./examples/model_epoch_043_ISICVal_prc.png" alt="validation prc model 026" style="width: 300px;"/> </td>
<td> <img src="./examples/model_epoch_043_ISICTest_prc.png" alt="test prc model 026" style="width: 300px;"/> </td>
</tr>
</table>

Statistics of both sets combined:

    Accuracy: 0.953015481784
    AUROC: ~0.95
    AUPRC: ~0.84
    Specificity/TNR: 0.9763548498446669, FPR: 0.023645150155333083
    PPV: 0.7944486121530383, NPV: 0.9711587982832618
    Sensitivity/TPR: 0.7591397849462366, FNR: 0.24086021505376343
    Informedness: 0.7354946347909035
    LR+: 32.10551762028101, LR-: 0.2466933155420727, Diag odds ratio: 130.14344369134528


#### "Bayesian" Joint Model

I found that by combining the models and letting `model_epoch_026.pth` predict first gave my model a better AUROC and sensitivity score. How it works is that model 026 predicts first, and if it believes that the sample is benign then it passes it to `model_epoch_043.pth` who then evaluates it again for the final prediction. Model 026 predicts first and we keep its malignant prediction because it has is overall better at identifying malignant samples.

Confusion matrices for validation and test sets:

<table>
<tr>
<td> <img src="./examples/joint/malig1st_malig026_val.png" alt="validation conf matrix model 026" style="width: 300px;"/> </td>
<td> <img src="./examples/joint/malig1st_malig026_test.png" alt="test conf matrix model 026" style="width: 300px;"/> </td>
</tr>
<tr>
<td> <img src="./examples/joint/malig1st_malig026_val_roc.png" alt="validation roc model 026" style="width: 300px;"/> </td>
<td> <img src="./examples/joint/malig1st_malig026_test_roc.png" alt="test roc model 026" style="width: 300px;"/> </td>
</tr>
<tr>
<td> <img src="./examples/joint/malig1st_malig026_val_prc.png" alt="validation prc model 026" style="width: 300px;"/> </td>
<td> <img src="./examples/joint/malig1st_malig026_test_prc.png" alt="test prc model 026" style="width: 300px;"/> </td>
</tr>
</table>

Statistics of both sets combined: 

    Accuracy: 0.94569822075
    AUROC: ~0.95
    AUPRC: ~0.83
    Specificity/TNR: 0.9615119088712462, FPR: 0.03848809112875384
    PPV: 0.718078381795196, NPV: 0.9772826945004824
    Sensitivity/TPR: 0.814336917562724, FNR: 0.18566308243727603
    Informedness: 0.77584882643397
    LR+: 21.158152916405506, LR-: 0.19309493800782215, Diag odds ratio: 109.5738352061223


## Interpretation of Model Diagnosis

### Diagnostic Performance

According to the study [“Detection of Malignant Melanoma Using Artificial Intelligence: An Observational Study of Diagnostic Accuracy” (2020)](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6936633/) naked-eye examination accuracy of melanocytic lesions by medical practicioners has the following statistics:

Primary Care Physicians:

    AUROC:        0.83
    Sensitivity:  0.799
    Specificity:  0.709

Dermatologists:

    AUROC:        0.91
    Sensitivity:  0.875
    Specificity:  0.814

The model in that paper (DERM) achieved an AUROC of 0.93, with sensitivity 0.95 and specificity 0.641. Their focus was to minimize false negatives. 

My best model (the joint model seen at ["Bayesian" Joint Model](#"bayesian"-joint-model)) performs better than the primary care physicians, and about as well as the dermatologists. Its sensitivity isn't as high as DERM, but its likely that this trade-off could be adjusted with fine-tuning the decision thresholds.

It might be noted that the diagnostic odds ratio of my model (109.6) is quite a bit higher than theirs at 95% specificity (38.3) showing a larger distinction between benign and malignant predictions. Overall I'm really happy with the performance of the joint model because overall reaching an AUROC of 0.95 is incredibly good, especially on what is considered to be a mobile neural network. 



### Making Diagnostic Inference 

Using the model for diagnostic inference is really easy, and I have given some examples at the beginning of the page. 

However, getting a prediction on an image isn't the full story, especially in a diagnostic context. One part of the model performance metrics that is important to this end are the LR scores (LR+/LR-). Using the LR values, we can directly calculate how the model's result affects our odds and/or probability of having a disease (the post-test odds). 

#### Calculating Disease Odds

Given our pretest odds, it is really easy to calculate the posttest odds given the prediction from our model. 

Let's say that the pretest probability of having melanoma for whites on average is 2.6% (from [here](https://www.cancer.org/cancer/melanoma-skin-cancer/about/key-statistics.html), not this is lifetime risk but I'm just using it as an example). Using our model 026 from above, the LR+ = 22.40, and the LR- = 0.21.

Given

<img src="https://latex.codecogs.com/png.image?\inline&space;\large&space;\dpi{110}\bg{white}pretest\_odds&space;=&space;\frac{pretest\_probability}{1&space;-&space;pretest\_probability}" title="https://latex.codecogs.com/png.image?\inline \large \dpi{110}\bg{white}pretest\_odds = \frac{pretest\_probability}{1 - pretest\_probability}" />

then we can have pretest odds = 0.02669

To find the posttest odds, we simply multiply the pretest odds by the LR value. If the test is a positive result, then this value is **22.4**. If the test is negative, it is **0.21**. We'll look at both outcomes.

If our test is positive then our posttest odds of having the disease is 0.598. If the test is negative then our posttest odds of having the disease is 0.0056.

Then, with 

<img src="https://latex.codecogs.com/png.image?\inline&space;\large&space;\dpi{110}\bg{white}posttest\_probability&space;=&space;\frac{posttest\_odds}{posttest\_odds&space;&plus;&space;1}" title="https://latex.codecogs.com/png.image?\inline \large \dpi{110}\bg{white}posttest\_probability = \frac{posttest\_odds}{posttest\_odds + 1}" />

we calculate that our posttest probability. For the positive test result this is 0.374, and for the negative test result this is 0.0056.

So our full story is that given the base probability of having melanoma is 2.6%, a positive image prediction raises that probability to **37.4%**, and a negative image prediction lowers that to **0.6%**. 

If you'd like to read more articles on using results from diagnostic tests, check out these:

 - [Bayes theorem and likelihood ratios for diagnostic tests](https://ekamperi.github.io/mathematics/2020/01/19/bayes-theorem-likelihood-ratios.html)
 - [Using Bayes’ rule in diagnostic testing: a graphical explanation](https://www.degruyter.com/document/doi/10.1515/dx-2017-0011/pdf)
 - [Likelihood ratios: Clinical application in day-to-day practice](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2683447/)

## Training the Model

To train the model yourself, you need to setup the ISIC dataset in the way the script expects, and then run `main.py` with the proper arguments. To prepare the dataset for training, see [Appendix A2: Preparing the ISIC Dataset for Training & Evaluation](#a2-preparing-the-isic-dataset-for-training--evaluation) and then come back here.

Once you have the dataset prepared in a structure like so:

    dataset_root
        /ISICTest
          /benign
          /malignant
        /ISICTrain
          /benign
          /malignant
        /ISICVal
          /benign
          /malignant

or a similar configuration (the key is just to have separate training and validation sets) then you can begin training with the main script. 

### Example

Assuming you have the dataset at ./dataset, your training command may look like this:

    python main.py --cuda --dataset_root ./dataset

This will start training the classifier model to predict for melanoma. There are many different command-line arguments that can be set such as batch size, learning rate, and so on. You can read more about those and how to use this script more under [Scripts - main.py](#mainpy).

Note: this script utilizes the `SummaryWriter` class from `torch.utils.tensorboard` for integration with Tensorboard. While it runs, it will save data into the `./runs` directory. If you wish to visualize it, and have tensorboard installed, just run `tensorboard --logdir runs` in a separate terminal while `main.py` is running.

----

## Scripts

### main.py

As discussed some in the [training example](#example) this script is used for training a new model. Here's the output of `python main.py -h`.

    usage: main.py [-h] [--image_size IMAGE_SIZE] [--batchSize BATCHSIZE]
                  [--valBatchSize VALBATCHSIZE] [--epochs EPOCHS] [--lr LR]
                  [--cuda] [--threads THREADS] [--seed SEED]
                  [--checkpoint_dir CHECKPOINT_DIR]
                  [--model_save_dir MODEL_SAVE_DIR] [--dataset_root DATASET_ROOT]
                  [--model_type MODEL_TYPE] [--weight_decay WEIGHT_DECAY]
                  [--weighted_sampling] [--weighted_loss]
                  [--load_checkpoint LOAD_CHECKPOINT] [--load_model LOAD_MODEL]
                  [--start_untrained]

#### main.py Arguments

- `--image_size`: Default = 512. Size which to scale down the maximum dimension of the input images.
- `--batchSize`: Default = 10. Training batch size. 
- `--valBatchSize`: Default = 15. Validation batch size.
- `--cuda`: Turn on CUDA.
- `--threads`: Default = 4. Number of threads with which to load training data.
- `--epochs`: Default = 50. Number of epochs to train.
- `--lr`: Default = 0.001. Learning rate.
- `--seed`: Default = 345. Random seed.
- `--checkpoint_dir`: Default = './checkpoints'. Directory to store checkpoint data.
- `--model_save_dir`: Default = './saved_models'. Directory to write out final trained models.
- `--dataset_root`: Default = './dataset'. Directory root for training/validation data. This folder should contain ISICTrain and ISICVal.
- `--model_type`: Default = 0 (MobileNetV2). 
  - 0 = MobileNetV2
  - 1 = EfficientNet-b0
  - 2 = EfficientNet-b1
  - ...
  - 7 = EfficientNet-b6
  - 8 = MobileNetV2
- `--weight_decay`: Default = 0.1. Amount of weight decay for AdamW optimizer.
- `--weighted_sampling`: Set to use weighted sampling from dataset.
- `--weighted_loss`: Set to use weighted cross-entropy loss.
- `--load_checkpoint`: Checkpoint *.pth file to load and resume training. Checkpoints have more information in them than the final model output.
- `--load_model`: Model file (*.pth) to load weights if starting training from another pre-trained model.
- `--start_untrained`: Set to not use pretrained weights in your loaded model when you start training.

#### Resuming training

If training fails for some reason, or for some reason you would like to resume training from a previously written checkpoint, the script can resume quickly given the checkpoint file. 
Example: 

    python main.py --cuda --load_checkpoint ./checkpoints/run###/model_epoch_##.pth

The ## can be replaced with your specific checkpoint path data. Doing this will resume training where it left off for the given checkpoint.

### runModel.py

`runModel.py` can be used to run the final trained model against any images/videos you would like and has different modes of output. Some examples can be seen at the top of this readme in [examples](#examples).

#### runModel.py Arguments

    usage: runModel.py [-h] [--cuda] --model_file MODEL_FILE
                      [--model_input_size MODEL_INPUT_SIZE] --input_file
                      INPUT_FILE [--output_file OUTPUT_FILE] [--in_type IN_TYPE]
                      [--seed SEED] [--verbose]


- `--model_file`: trained model file to run.
- `--model_input_size`: Default = 512. The input image is bound to this as the maximum width or height dimension. If it is above this on either axis, it is scaled down before inference.
- `--input_file`: media input file on which to run the model. If it is a video, you must specify `--in_type v`.
- `--in_type`: Media input type. `v` for video, `img` for images, `w` for webcam.
- `--output_file`: Supply this if you would like to write out the modified media to disk.
- `--verbose`: Set to get more debug info

### testSetAccuracy.py

Use this script to test the overall stats of a collection of models (probably checkpoints). When run and compared using a test dataset, this may give another metric for comparing models. The output of this script is a yaml file that is written into the ./stats directory, named using a timestamp.

#### testSetAccuracy.py Arguments

    usage: testSetAccuracy.py [-h] [--cuda] --checkpoint_folder CHECKPOINT_FOLDER
                              [--dataset_root DATASET_ROOT]
                              [--model_input_size MODEL_INPUT_SIZE]
                              [--num_images NUM_IMAGES] [--seed SEED]

- `--checkpoint_folder`: Providing this will have the script test, and calculate metrics for each model in the folder.
- `--dataset_root`: Default = './dataset'. Root for the prepared ISIC dataset files. Tests against \<root>/ISICTest
- `--model_input_size`: Default = 512. The input image is bound to this as the maximum width or height dimension. If it is above this on either axis, it is scaled down before inference.
- `--num_images`: Default is full dataset. Set to run on a smaller subset of the dataset.

#### testSetAccuracy.py Example

    python testAccuracy.py --cuda --checkpoint_folder ./checkpoints/<checkpoint_folder> --dataset_root ./datasets/SegDataset/ --num_images 1000

Output:

A yaml file with a timestamp is written out to the ./stats directory. 

Example: `testSet20220531_175514.yml`, first 25 lines:

    checkpointDirectory: ./checkpoints/run2022428_10149/
    inputDirectory: ./datasets/AugDataset/ISICVal
    model_epoch_000.pth:
      Diag odds ratio: 62.850571027850016
      FNR: 0.38407079646017694
      FPR: 0.024881003894418052
      LR+: 24.7549980761831
      LR-: 0.39387069475015263
      NPV: 0.9540643522438611
      PPV: 0.7516198704103672
      accuracy: 0.9359938307306729
      auprc: 0.7604338418083273
      auroc: 0.9438540378260187
      conf_matrix:
      - - 9014.0
        - 230.0
      - - 434.0
        - 696.0
      informedness: 0.5910481996454049
      sensitivity/TPR: 0.6159292035398231
      specificity/TNR: 0.975118996105582
    model_epoch_001.pth:
      Diag odds ratio: 64.30841535527853
      FNR: 0.34159292035398225
      FPR: 0.029099956728688925

### testFinalAccuracy.py

Gives useful performance metrics and graphs for a model. Can also be used to test as a simple 'joint' model. This is what I do for the first example given in the readme. In that case, give a 'benign' model with `--model_benign` and a 'malignant' model with `--model_malignant`. Each just means that the model is a bit better at predicting benign/malignant than the other.

    usage: testFinalAccuracy.py [-h] [--cuda] [--model MODEL]
                            [--model_benign MODEL_BENIGN]
                            [--model_malignant MODEL_MALIGNANT]
                            [--dataset_root DATASET_ROOT]
                            [--model_input_size MODEL_INPUT_SIZE]
                            [--seed SEED] [--show_roc] [--show_pr]


- `--model`: Path to a trained model file.
- `--model_benign`: Path to a more benign trained model file.
- `--model_malignant`: Path to a more malignant trained model file.
- `--dataset_root`: Root of your dataset test set to use. 
- `--model_input_size`: Default = 512. Input size to the model.
- `--show_roc`: Show AUROC curve after testing.
- `--show_pr`: Show AUPRC curve after testing.

#### Output: 

Example: `python testFinalAccuracy.py --cuda --model ./saved_models/model_epoch_026.pth --dataset_root ./datasets/AugDataset/ISICTest --show_roc --show_pr`

    [[2235   97]
    [  45  232]]
    AUROC: 0.9531839235623037
    Accuracy: 0.9455730164814105
    PR AUC: 0.8324492280392041
    Specificity/TNR: 0.9584048027444254, FPR: 0.04159519725557459, PPV: 0.7051671732522796, NPV: 0.9802631578947368
    Sensitivity/TPR: 0.8375451263537906, FNR: 0.16245487364620936, Informedness: 0.7959499290982159
    LR+: 20.135620975845782, LR-: 0.1695054878491992, Diag odds ratio: 118.79037800687296

![Example confusion matrix](./examples/model_epoch_026_ISICVal_conf.png)
![Example AUROC curve](./examples/model_epoch_026_ISICVal_roc.png)
![Example AUPRC curve](./examples/model_epoch_026_ISICVal_prc.png)

### constructDataset.py

This script organizes the ISIC data in a way that can be used with the other scripts in this project for training and testing a the classification model. You can read more about the dataset structure under [Appendix - A2. Preparing the ISIC Dataset for Training & Evaluation](#a2-preparing-the-isic-dataset-for-training--evaluation).

#### constructDataset.py Arguments

    usage: constructDataset.py [-h] [--root ROOT] [--percentVal PERCENTVAL]
                              [--percentTest PERCENTTEST] [--output OUTPUT]
                              [--model MODEL] [--cuda] [--show] [--mask] [--crop]
                              [--num_images NUM_IMAGES]
                              [--model_input_size MODEL_INPUT_SIZE]
                              [--skipnotfound]
                              [--max_output_size MAX_OUTPUT_SIZE]

- `--root`: Default = './ISIC'. Root of the downloaded ISIC archive dataset.
- `--percentVal`: Default = 0.2. Percent of the dataset to set as validation data.
- `--percentTest`: Default = 0.05. Percent of the dataset to set as test data.
- `--output`: Default = './dataset'. Root of the output dataset with train, val, and test data.
- `--model`: Path to the trained segmentation model that will process the dataset (crop the images).This will only be used if you set `--mask` or `--crop`. See my other repository to obtain a trained model or to train your own: [Dermascopic Melanoma Image Segmentation in PyTorch](https://gitlab.com/dibz15/dermascopic-melanoma-image-segmentation-in-pytorch).
- `--show`: Used for debugging. Will show each image as its processed.
- `--mask`: Use the given segmentation model to mask the found moles.
- `--crop`: Use the given segmentation model to crop the image down to just the mole area. In my opinion this is the most useful option.
- `--num_images`: Number of images to use from ISIC to construct the dataset. If not set, uses the full dataset.
- `--model_input_size`: Default = no max size. Limits the max image dimension as it goes into the model. If set, I recommend 512.
- `--skipnotfound`: Only useful when providing a segmentation model. Skip images where a lesion isn't found in the image by the segmentation model. I recommend using this with the model as it increases output quality.
- `--max_output_size`: The max size of an image's dimension (width or height) when outputting. Useful for decreasing the output size of the overal dataset. I recommend a max of 1024.

Output:

Under the given `--output` folder - 

    \ISICTrain
      \benign
      \malignant
    \ISICVal
      ...
    \ISICTest
      ...

### augmentDataset.py 

This script can be used to make the dataset more balanced by oversampling the malignant examples in the dataset. Its root should be something like ./dataset/ISICTrain and output something like ./aug_dataset/ISICTrain.


#### augmentDataset.py Arguments
    usage: augmentDataset.py [-h] --root ROOT --output OUTPUT [--show]
                            --min_percent MIN_PERCENT

- `--root`: Root directory to pull samples from. Should be something like ./dataset/ISICTrain or ./dataset/ISICVal
- `--output`: Output directory root. Should be something like ./aug_dataset/ISICTrain or ./aug_dataset/ISICVal
- `--show`: Shows examples
- `--min_percent`: The minimum percent of malignant examples. Should be [0, 0.5].

-----

## Appendix

### A1. ISIC Archive Downloader

For this project, I utilized the scripts at [https://github.com/GalAvineri/ISIC-Archive-Downloader](https://github.com/GalAvineri/ISIC-Archive-Downloade). If you would like to use the same script to download the dataset yourself, the descriptions, images, and segmentation masks can all be downloaded with this line:

    python download_archive.py -s

And note that it by default downloads everything into the `./dataset` folder wherever you launch the script. I've found that the total downloaded dataset is > _100GiB_, so make sure you have enough space!

### A2. Preparing the ISIC Dataset for Training & Evaluation

The scripts `main.py` and `testSetAccuracy.py` in this project expect the ISIC dataset to be split into train, val, and test datasets so that training and inference can take place. These datasets are expected to be in a directory structure like this:

    dataset_root
      \ISICTrain
        \benign
        \malignant
      \ISICVal
        ...
      \ISICTest
        ...

Under each ISIC* subdirectory, there are two directories sorting the examples into benign and malignant. This follows the ImageFolder hierarchy from PyTorch.

To build this dataset, you can simply run the `constructDataset.py` script with the proper arguments and it will organize the dataset as specified above.

Example:

    python constructDataset.py --root ./ISIC --percentVal 20 --percentTest 10 --output ./dataset

It can also be run without arguments, using the default options. You can read more about the options for this command above under [Scripts - constructDataset.py](#constructdatasetpy).


After constructing the sorted dataset, it can further be augmented to oversample the malignant examples into a new dataset. See [Scripts - augmentDataset.py](#augmentdatasetpy) above.

### A3. Intersection Over Union (IoU)

Intersection over Union bounding box comparison considers a bounding box overlapping another bounding box if their 'IoU' value is greather than some threshold. For IoU threshold 0.4 this means that if the predicted box is overlapping the ground truth by at least 40% then it is considered a true positive. If a box overlaps less than 40% it is a false positive, and if there is a ground truth box without an overlapping predicted box then it is a false negative. Everything else would be a true negative.

Read more [here](https://towardsdatascience.com/intersection-over-union-iou-calculation-for-evaluating-an-image-segmentation-model-8b22e2e84686).

The threshold for this IoU calculation is set with the commandline argument `--iou_threshold`. See [Commandline Arguments](#commandline-arguments). 
A useful comparison measurement for object detection models is mAP (mean average precision) of a model. Often mAP is charted vs. IoU.

Read more about mAP [here](https://towardsdatascience.com/implementation-of-mean-average-precision-map-with-non-maximum-suppression-f9311eb92522).


### A4. Further Reading

- [Detection of Malignant Melanoma Using Artificial Intelligence: An Observational Study of Diagnostic Accuracy](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6936633/)
- [Bayes theorem and likelihood ratios for diagnostic tests](https://ekamperi.github.io/mathematics/2020/01/19/bayes-theorem-likelihood-ratios.html)
- [Using Bayes' rule in diagnostic testing: a graphical explanation](https://www.degruyter.com/document/doi/10.1515/dx-2017-0011/pdf)
- [Key Statistics for Melanoma Skin Cancer](https://www.cancer.org/cancer/melanoma-skin-cancer/about/key-statistics.html)
- [Likelihood ratios: Clinical application in day-to-day practice](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2683447/)
- [What is a ROC Curve and How to Interpret It](https://www.displayr.com/what-is-a-roc-curve-how-to-interpret-it/)
- [Measuring Performance: AUPRC and Average Precision](https://glassboxmedicine.com/2019/03/02/measuring-performance-auprc/)