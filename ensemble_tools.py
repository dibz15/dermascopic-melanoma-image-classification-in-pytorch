
import numpy as np
import torchvision
import torch
from torch import nn
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection.faster_rcnn import FasterRCNN
from torchvision.models.detection.rpn import AnchorGenerator
from torch.utils.data import SubsetRandomSampler

from torchvision import transforms

from PIL import Image
import math, time, copy, os, random
import cv2
import matplotlib.pyplot as plt

from utils import *

from torch.utils.tensorboard import SummaryWriter 

import argparse

from torchvision.datasets import ImageFolder

from sklearn.metrics import roc_curve, auc

from tqdm import tqdm

from efficientnet_pytorch import EfficientNet

from model_trainer import train_model, getTrainingTransforms, getValidationTransforms, imshow

from MyEnsemble import MyEnsemble

def loadEnsembleModel(ensemblePaths, device):
    head_models = []
    largest_model_type = 0
    ensemble_types = []
    for path in ensemblePaths:

        checkpointDict = loadModelCheckpointDict(path, device)
        if 'model_type' not in checkpointDict or checkpointDict['model_type'] < 1:
            print('Error: Wrong model type in {}'.format(path))
            exit(1)

        largest_model_type = max(largest_model_type, checkpointDict['model_type'])
        ensemble_types.append(checkpointDict['model_type'])

        _, _, trainableModel = loadModelFromCheckpoint(checkpointDict, None, None)

        for param in trainableModel.parameters():
            param.requires_grad = False
        # ct = 0
        # for name, param in trainableModel.named_parameters():
        #     ct += 1
        #     if ct < 150:
        #         param.requires_grad = False
        #     else:
        #         param.requires_grad = True

        trainableModel = trainableModel.to(device)
        head_models.append(trainableModel)

    ensembleModel = MyEnsemble(2, head_models)
    ensembleModel = ensembleModel.to(device)

    return ensembleModel, largest_model_type, ensemble_types


def loadEnsembleFromCheckpoint(checkpointDict, optimizer, scheduler, criterion=None, dropout=0.4):
    checkpoint = checkpointDict

    models = []
    for model_type, model_state in zip(checkpoint['ensemble_types'], checkpoint['model_state']):
        model = getEfficientNetClassifier(num_classes=2, size='b{}'.format(model_type-1), dropout=dropout)
        model._fc = None
        model.load_state_dict(model_state)
        for param in model.parameters():
            param.requires_grad = False
        model = model.to(device)
        models.append(model)

    ensembleModel = MyEnsemble(2, models)
    ensembleModel.load_state_dict(checkpoint['ensemble_state'])

    if optimizer is not None and checkpoint['optimizer_state'] is not None:
        optimizer.load_state_dict(checkpoint['optimizer_state'])

    if scheduler is not None and checkpoint['scheduler_state'] is not None:
        scheduler.load_state_dict(checkpoint['scheduler_state'])

    if criterion is not None and checkpoint['criterion_state'] is not None:
        criterion.load_state_dict(checkpoint['criterion_state'])

    return checkpoint['epoch'], checkpoint['loss'], ensembleModel, checkpoint['ensemble_types']

def saveEnsembleCheckpoint(epoch, loss, ensembleModel: MyEnsemble, ensemble_types, optimizer, scheduler, path, criterion=None):
    torch.save({
        'epoch': epoch,
        'model_state': [model.state_dict() for model in ensembleModel.models],
        'ensemble_state': ensembleModel.state_dict(),
        'ensemble_types': ensemble_types,
        'optimizer_state': optimizer.state_dict(),
        'scheduler_state': scheduler.state_dict() if scheduler is not None else None,
        'criterion_state': criterion.state_dict() if criterion is not None else None,
        'loss': loss
    }, path)


def train_ensemble(model, dataloaders, optimizer, scheduler, criterion, device, ensemble_types, num_epochs=25, checkpoint_dir=None, start_epoch=0):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    writer = SummaryWriter()

    for epoch in tqdm(range(start_epoch, num_epochs + start_epoch)):
        epoch_since = time.time()
        # print('Epoch {}/{}'.format(epoch, num_epochs - 1 + start_epoch))
        # print('-' * 10)

        writer.add_scalar('memory/allocated', torch.cuda.memory_allocated(), epoch)
        writer.add_scalar('memory/cached', torch.cuda.memory_cached(), epoch)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            epoch_loss = 0
            epoch_acc = 0
            epoch_auroc = 0
            iterations = 0
            batchNum = 0
            for images, labels in tqdm(dataloaders[phase]):
                #print('loop', phase)

                images = images.to(device)
                labels = labels.to(device)

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    # zero the parameter gradients
                    optimizer.zero_grad()

                    outputs = model(images)
                    loss = criterion(outputs, labels)

                    #print('{} Batch {}/{}'.format(phase, batchNum, len(dataloaders[phase]) - 1))
                    batchNum += 1

                    if not math.isfinite(loss.item()):
                        print("Loss is {} during {}".format(loss.item(), phase))
                        if phase == 'train':
                            print('Quitting!')
                            quit()

                    #losses = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                    epoch_loss += loss.item()
                    preds = torch.max(outputs, 1)[1]
                    gt = labels.data

                    fpr, tpr, thresholds = roc_curve(y_true = gt.cpu().numpy(), y_score = preds.cpu().numpy(), pos_label = 1) #positive class is 1; negative class is 0
                    auroc = auc(fpr, tpr)

                    epoch_acc += torch.sum(preds == gt).double() / len(outputs)
                    epoch_auroc += auroc

            lossloss = epoch_loss / len(dataloaders[phase])
            epoch_acc = epoch_acc / len(dataloaders[phase])
            epoch_auroc = epoch_auroc / len(dataloaders[phase])

            if phase == 'train' and scheduler:
                scheduler.step()

            writer.add_scalar('loss/{}'.format(phase), lossloss, epoch)
            writer.add_scalar('accuracy/{}'.format(phase), epoch_acc, epoch)
            writer.add_scalar('auroc/{}'.format(phase), epoch_auroc, epoch)

            # deep copy the model
            # if phase == 'val' and epoch_acc > best_acc:
            #     best_model_wts = copy.deepcopy(model.state_dict())
            if checkpoint_dir is not None and (epoch + 1) % 1 == 0 and phase == 'val':
                print('Checkpoint saving for epoch {:03d}.'.format(epoch))
                saveEnsembleCheckpoint(epoch, 
                                    epoch_loss, 
                                    model, 
                                    ensemble_types,
                                    optimizer, 
                                    scheduler, 
                                    os.path.join(checkpoint_dir, 'model_epoch_{:03d}.pth'.format(epoch)),
                                    criterion)
        time_elapsed = time.time() - epoch_since
        #print('Epoch completed in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))

    print()
    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))

    writer.flush()
    writer.close()

    # load best model weights
    # model.load_state_dict(best_model_wts)
    return model