import torchvision
import torch
from torch import nn
import os
from utils import *
import argparse
from torchvision.datasets import ImageFolder
from efficientnet_pytorch import EfficientNet

from model_trainer import train_model, getTrainingTransforms, getValidationTransforms, imshow

parser = argparse.ArgumentParser(description='Classification on ISIC Dataset.')
parser.add_argument('--image_size', type=float, default=512, help="Size to crop/scale all input images")
parser.add_argument('--batchSize', type=int, default=10, help='training batch size')
parser.add_argument('--valBatchSize', type=int, default=15, help='validation batch size')
parser.add_argument('--epochs', type=int, default=50, help='number of epochs to train for')
parser.add_argument('--lr', type=float, default=0.001, help='Learning Rate. Default=0.01')
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--threads', type=int, default=4, help='number of threads for data loader to use')
parser.add_argument('--seed', type=int, default=456, help='random seed to use. Default=123')
parser.add_argument('--checkpoint_dir', type=str, default=os.path.join('.', 'checkpoints'), help="directory to save checkpoints into")
parser.add_argument('--model_save_dir', type=str, default=os.path.join('.', 'saved_models'), help="directory to save final models into")
parser.add_argument('--dataset_root', type=str, default=os.path.join('.', 'dataset'), help='directory path to dataset root')
parser.add_argument('--model_type', type=int, default=0, help='Model type to load when creating a new model. 0 - MobileNet V2. 1 - EfficientNet-b0. 2 - EfficientNet-b1.')
parser.add_argument('--weight_decay', type=float, default=0.1, help='Amount of weight decay for AdamW optimizer.')
parser.add_argument('--weighted_sampling', action='store_true', help='use weighted sampling')
parser.add_argument('--weighted_loss', action='store_true', help='use weighted cross-entropy loss')
# parser.add_argument('--train_num_images', type=int, default=-1, help='Number of training images to use from the set. Default is to use the whole dataset.')
# parser.add_argument('--val_num_images', type=int, default=-1, help='Number of validation images to use from dataset. Default is to use the whole dataset.')

parser.add_argument('--load_checkpoint', type=str, help="model file to load if resuming training")
parser.add_argument('--load_model', type=str, help="model file to load if training from custom pre-trained model")
parser.add_argument('--start_untrained', action='store_true', help='Load an untrained model to start?')

opt = parser.parse_args()

if opt.model_type:
    if opt.model_type >= 1 and opt.model_type < 8:
        opt.image_size = EfficientNet.get_image_size('efficientnet-b{}'.format(opt.model_type-1))
        pass

os.makedirs(opt.model_save_dir, exist_ok=True)
checkpoint_dir = os.path.join(opt.checkpoint_dir, 'run{}'.format(getTimestamp()))
os.makedirs(checkpoint_dir, exist_ok=True)

#Make RNG deterministic
torch.manual_seed(opt.seed)
np.random.seed(opt.seed)
if torch.cuda.is_available():
    torch.cuda.manual_seed_all(opt.seed)

device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')
lr = opt.lr
num_epochs=opt.epochs
CELossWeights = None

train_dataset = ImageFolder(os.path.join(opt.dataset_root, 'ISICTrain'), getTrainingTransforms(opt.image_size))
val_dataset = ImageFolder(os.path.join(opt.dataset_root, 'ISICVal'), getValidationTransforms(opt.image_size))

train_weighted_sampler = None
if opt.weighted_sampling:
    train_weighted_sampler = getWeightedSampler(train_dataset)
elif opt.weighted_loss:
    CELossWeights = getCrossEntropyLossWeights(get_class_distribution(train_dataset))
    print(f'Using weights for CE loss: {CELossWeights}')

trainLoader = torch.utils.data.DataLoader(train_dataset, 
                                            batch_size=opt.batchSize, 
                                            shuffle=(not opt.weighted_sampling), 
                                            num_workers=opt.threads,
                                            sampler=train_weighted_sampler)

valLoader = torch.utils.data.DataLoader(val_dataset, 
                                            batch_size=opt.valBatchSize, 
                                            shuffle=(not opt.weighted_sampling), 
                                            num_workers=opt.threads)

class_names = train_dataset.classes

dataloaders = { 
    'train': trainLoader,
    'val': valLoader
}

# Get a batch of training data
inputs, classes = next(iter(dataloaders['train']))

# Make a grid from batch
out = torchvision.utils.make_grid(list(inputs))

imshow(out, title=[class_names[x] for x in classes])

startEpoch = 0

print()
if opt.load_model is not None:
    print('Loading previously trained model', opt.load_model)

    checkpointDict = loadModelCheckpointDict(opt.load_model, device)
    opt.model_type = checkpointDict['model_type']
    _, _, trainableModel = loadModelFromCheckpoint(checkpointDict, None, None)

    trainableModel = trainableModel.to(device)

    optimizer = torch.optim.AdamW(trainableModel.parameters(), lr=lr, weight_decay=opt.weight_decay )
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=5, gamma=0.7)

    criterion = nn.CrossEntropyLoss(weight=CELossWeights)

elif opt.load_checkpoint is not None:

    print('Loading model from previously saved checkpoint', opt.load_checkpoint)

    checkpointDict = loadModelCheckpointDict(opt.load_checkpoint, device)
    opt.model_type = checkpointDict['model_type']
    startEpoch, _, trainableModel = loadModelFromCheckpoint(checkpointDict, None, None)

    params = [p for p in trainableModel.parameters() if p.requires_grad]

    optimizer = torch.optim.AdamW(params, lr=lr, weight_decay=opt.weight_decay )
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=5, gamma=0.7)

    criterion = nn.CrossEntropyLoss(weight=CELossWeights)

    trainableModel = trainableModel.to(device)
    loadModelFromCheckpoint(checkpointDict, optimizer, lr_scheduler, criterion)

    print('Starting at epoch', startEpoch)

else:
    loadPretrained = not opt.start_untrained
    if opt.model_type is None or opt.model_type == 0:
        print('Creating new model template for MobileNet_V2')
        trainableModel = getMobileNetV2Classifier(num_classes=2, pretrained=loadPretrained)
        print(f'Model size: {sum(p.numel() for p in trainableModel.parameters())}')
    elif opt.model_type is not None and opt.model_type >= 1 and opt.model_type < 8:
        print('Creating new model template for EfficientNet-b{}'.format(opt.model_type-1))
        trainableModel = getEfficientNetClassifier(num_classes=2, size='b{}'.format(opt.model_type-1), pretrained=loadPretrained)
        print(f'Model size: {sum(p.numel() for p in trainableModel.parameters())}')
    elif opt.model_type is not None and opt.model_type >= 8:
        print('Creating model MobileNet V3')
        trainableModel = getMobileNetV3Classifier(num_classes=2, pretrained=loadPretrained)
        print(f'Model size: {sum(p.numel() for p in trainableModel.parameters())}')

    trainableModel = trainableModel.to(device)

    for param in trainableModel.parameters():
        param.requires_grad = True

    optimizer = torch.optim.AdamW(trainableModel.parameters(), lr=lr, weight_decay=opt.weight_decay )
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=5, gamma=0.7)

    criterion = nn.CrossEntropyLoss(weight=CELossWeights)

criterion = criterion.to(device)

print('Run hyper-Parameters:')
hparams = {}
print('\tImage Size:', opt.image_size)
hparams['imgSize'] = opt.image_size
print('\tTrain Batch Size:', opt.batchSize)
hparams['batchSize'] = opt.batchSize
print('\tVal Batch Size:', opt.valBatchSize)
hparams['valBatchSize'] = opt.valBatchSize
print('\tEpochs:', opt.epochs)
print('\tLR:', opt.lr)
hparams['lr'] = opt.lr
print('\tDevice:', device)
print('\tThreads:', opt.threads)
print('\tModel type: ', opt.model_type)
hparams['type'] = opt.model_type
hparams['weighted_sampling'] = opt.weighted_sampling
hparams['weighted_loss'] = opt.weighted_loss
print('\tWeighted sampling: ', opt.weighted_sampling)
print('\tWeighted loss: ', opt.weighted_loss)

print('Starting at epoch', startEpoch)

print()

try:
    best_model = train_model(trainableModel, dataloaders, optimizer, lr_scheduler, criterion, device, opt.model_type, num_epochs, checkpoint_dir=checkpoint_dir, start_epoch=startEpoch, hparams=hparams)

    saveFileName = 'final_model_isic{}.pth'.format(getTimestamp())
    saveFilePath = os.path.join(opt.model_save_dir, saveFileName)
    torch.save({'model_state' : best_model.state_dict()}, saveFilePath)
    print('Model file saved to {}'.format(saveFilePath))
except KeyboardInterrupt:
    print('Keyboard interrupt. Quitting...')

saveModelCheckpoint(num_epochs + startEpoch - 1, 
                    0, 
                    trainableModel, 
                    optimizer, 
                    lr_scheduler, 
                    os.path.join(checkpoint_dir, 'model_final.pth'),
                    criterion,
                    opt.model_type)