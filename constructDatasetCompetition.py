

import os
import argparse

import numpy as np
import pandas as pd

from shutil import copy2
from shutil import rmtree

import cv2

import json
import ntpath

from utils import *

import torch

from tqdm import tqdm


parser = argparse.ArgumentParser(description='Construct a dataset usable for segmentation from ISIC')
parser.add_argument('--root', type=str, default='./ISIC_Competition', help='Root directory of ISIC dataset.')
parser.add_argument('--meta_file', type=str, required=True, help='.csv file with metadata for classifying images.')
parser.add_argument('--percentVal', type=float, default=0.2, help="Proportion of total dataset that will become validation")
parser.add_argument('--output', type=str, default='./ClassDatasetCompetition', help='Output directory path to create new segmentation dataset')
parser.add_argument('--model', type=str, help='Model file for segmentation')
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--show', action='store_true', help='show images?')
parser.add_argument('--mask', action='store_true', help='mask images?')


opt = parser.parse_args()

if opt.mask and not opt.model:
    print('Must supply masking model if using masking.')
    exit(1)

if opt.percentVal >= 1.0:
    print('Validation and Test percentage shouldn\'t exceed the whole.')
    exit(1)

if opt.percentVal <= 0.0:
    print('Validation set size must be greater than 0.0')
    exit(1)

print('Loading Segmentation model...')

if opt.model:
    device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')
    model = torch.load(opt.model, map_location=device)

    if type(model) == dict:
        if 'full_model' in model.keys() and model['full_model'] is not None:
            model = model['full_model']
        elif 'model_state' in model.keys() and model['model_state'] is not None:
            model = getModifiedMaskRCNN(backbone_state=model['backbone_state'], model_state=model['model_state'])

    model = model.to(device)
    model.eval()
else:
    model = None

# Create a list that pairs together the Segmentation (paths) with their images (paths)

# Separate the list into training and validation and training

# Create a new dataset folder, perhaps called SegDataset, with subfolders 
# ISICTrain, and ISICVal, and ISICTrain

# Copy the images using their paths into their new appropriate folders.
# Under each set folder should be also the folders Images and Segmentations.
# For example, ISICTrain would have ISICTrain/Images and ISICTrain/Segmentations

def findImagePaths(root, subFolder, blackList=None):
    basePath = os.path.join(root, subFolder)

    categoryFolder_files = set(os.listdir(basePath))

    categoryFolder_paths = [str(os.path.join(basePath, categoryFile)) for categoryFile in categoryFolder_files]
    imagePaths = sorted(categoryFolder_paths)

    return imagePaths


def subsetPaths(pathPairs, percentVal):
    assert(percentVal <= 1.0)

    totalNum = len(pathPairs)
    numVal = int(float(totalNum) * percentVal)
    numTrain = totalNum - numVal

    indexList = list(range(len(pathPairs)))
    indexList = np.array(indexList)
    np.random.shuffle(indexList)

    trainPairs = []
    for index in indexList[0:numTrain]:
        trainPairs.append(pathPairs[index])

    valPairs = []
    for index in indexList[numTrain:numTrain+numVal]:
        valPairs.append(pathPairs[index])

    return trainPairs, valPairs


def gatherMalignancy(csvFile):

    dataset = pd.read_csv(csvFile)
    df = pd.DataFrame(dataset)

    malDict = { fileName: int(target) for fileName, target in zip(df['image_name'], df['target'])}

    return malDict

print('Collecting image paths...')
imagePaths = findImagePaths(opt.root, 'train')
malignancyDict = gatherMalignancy(opt.meta_file)

print('Separating dataset into Train and Validation...')
print('\tTotal Valid Dataset Size:', len(imagePaths))
print('\tValidation Proportion:', opt.percentVal)
trainPaths, valPaths = subsetPaths(imagePaths, opt.percentVal)
print()
print('\tFinal Train Size:', len(trainPaths))
print('\tFinal Val Size:', len(valPaths))

os.makedirs(opt.output, exist_ok=True)

def getMalignancy(filename, malignancyDict):
    subname = filename.split('.')[0]

    if subname in malignancyDict:
        return malignancyDict[subname]

    return 0


def cleanDataset(root, subFolder):
    subPath = os.path.join(root, subFolder)    
    malignantPath = os.path.join(subPath, 'malignant')
    benignPath = os.path.join(subPath, 'benign')

    if os.path.exists(malignantPath):
        rmtree(malignantPath)
    
    if os.path.exists(benignPath):
        rmtree(benignPath)

def maskImage(model, image):
    if model is None:
        return None
    modelInput = [getTestTransforms()(image).to(device)]
    pred = model(modelInput)
    sortedDict = sortPredictionsByClass(pred, 1)

    boxes = sortedDict[1]
    boxPicks = nonMaximumSuppression_pick(boxes, confThresh=0.2, iouThresh=0.0)
    boxes = boxes[boxPicks]
    if (len(boxPicks) > 0):
        img = image
        #cvPutBoxes(img, boxes)

        cleanedMasks = cleanupMasks(pred)
        maskPicks = cleanedMasks[0][boxPicks]

        if len(boxPicks) > 1:
            bestBoxIdx = np.argmax(boxes[:, 4], axis=0)
        else:
            bestBoxIdx = 0

        mask = maskPicks[bestBoxIdx] * 255

        mask[np.where(mask >= 127)] = 255
        mask[np.where(mask < 127)] = 0
        mask = np.array(mask, dtype=np.uint8)

        maskRGB = cv2.cvtColor(mask, cv2.COLOR_GRAY2RGB)

        maskedImg = cv2.bitwise_and(img, maskRGB)

        return maskedImg
    else:
        return None

def makeDataset(root, subFolder, paths, model, malignancyDict):
    subPath = os.path.join(root, subFolder)
    os.makedirs(subPath, exist_ok=True)
    
    malignantPath = os.path.join(subPath, 'malignant')
    benignPath = os.path.join(subPath, 'benign')

    os.makedirs(malignantPath, exist_ok=True)
    os.makedirs(benignPath, exist_ok=True)

    #Write out the pair paths into a file
    for path in tqdm(paths):

        #print(pair[1])
        basename = ntpath.basename(path)
        malignancy = getMalignancy(basename, malignancyDict)

        if malignancy == 0:
            outputImagePath = os.path.join(benignPath, basename)
        elif malignancy == 1:
            outputImagePath = os.path.join(malignantPath, basename)

        #Read CV2 Image
        g_img = cv2.imread(path)

        if opt.mask:
            maskedImage = maskImage(model, g_img)
            if maskedImage is None:
                continue

            if opt.show:
                cv2.imshow('m', g_img)
                cv2.imshow('masked', maskedImage)
                cv2.waitKey()
        else:
            maskedImage = g_img

        cv2.imwrite(outputImagePath, maskedImage)

print('The following directories will be created and overwritten:')

print(opt.output)
print('\t> ISICTrain')
print('\t\t> benign')
print('\t\t> malignant')
print('\t> ISICVal')
print('\t\t> benign')
print('\t\t> malignant')

    
print('\nThis CANNOT BE UNDONE!')

response = input('\nAre you sure? (Y\\n):')

if response.lower() != 'y':
    print('Exiting...')
    exit(0)

print('Cleaning ISICTrain...')
cleanDataset(opt.output, 'ISICTrain')

print('Cleaning ISICVal')
cleanDataset(opt.output, 'ISICVal')


print('Constructing ISICTrain...')
makeDataset(opt.output, 'ISICTrain', trainPaths, model, malignancyDict)
print('Constructing ISICVal...')
makeDataset(opt.output, 'ISICVal', valPaths, model, malignancyDict)
