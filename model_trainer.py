import numpy as np
import torchvision
import torch
from torch import nn
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection.faster_rcnn import FasterRCNN
from torchvision.models.detection.rpn import AnchorGenerator
from torch.utils.data import SubsetRandomSampler

from torchvision import transforms

from PIL import Image
import math, time, copy, os, random
import matplotlib.pyplot as plt

from utils import *

from torch.utils.tensorboard import SummaryWriter 

from sklearn.metrics import roc_curve, auc

from tqdm import tqdm

def getTrainingTransforms(image_size):
    return transforms.Compose([
        # transforms.ColorJitter(brightness=0.1, contrast=0.05, saturation=0.05, hue=0.05),
        # transforms.RandomCrop(size=opt.image_size),
        transforms.Resize((image_size, image_size)),
        #transforms.RandomApply([transforms.RandomRotation(degrees=30)], p=0.3),
        # transforms.RandomAffine(degrees=30, translate=(0.15, 0.15), scale=(0.8, 1.0), shear=1),
        transforms.ToTensor(),
        transforms.Normalize(mean_sums, std_sums)
    ])

def getValidationTransforms(image_size):
    return transforms.Compose([
        transforms.Resize((image_size, image_size)),
        #transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean_sums, std_sums)
    ])

    
def visualize_model(model, class_names, num_images=6):
    was_training = model.training
    model.eval()
    images_so_far = 0
    fig = plt.figure()

    with torch.no_grad():
        for i, (inputs, labels) in enumerate(dataloaders['val']):
            inputs = inputs.to(device)
            labels = labels.to(device)

            outputs = model(inputs)
            _, preds = torch.max(outputs, 1)

            for j in range(inputs.size()[0]):
                images_so_far += 1
                ax = plt.subplot(num_images//2, 2, images_so_far)
                ax.axis('off')
                ax.set_title('predicted: {}'.format(class_names[preds[j]]))
                imshow(inputs.cpu().data[j])

                if images_so_far == num_images:
                    model.train(mode=was_training)
                    return
        model.train(mode=was_training)

def imshow(inp, title=None):
    """Imshow for Tensor."""
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.show()

def train_model(model, dataloaders, optimizer, scheduler, criterion, device, model_type, num_epochs=25, checkpoint_dir=None, start_epoch=0, hparams : dict = {}):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    writer = SummaryWriter(comment=f'{hparams}')

    # tmpimages, _ = next(iter(dataloaders['train']))
    # grid = torchvision.utils.make_grid(tmpimages)
    # writer.add_image("images", grid)
    # writer.add_graph(model, tmpimages.to(device))

    for epoch in tqdm(range(start_epoch, num_epochs + start_epoch)):
        epoch_since = time.time()
        # print('Epoch {}/{}'.format(epoch, num_epochs - 1 + start_epoch))
        # print('-' * 10)

        writer.add_scalar('memory/allocated', torch.cuda.memory_allocated(), epoch)
        writer.add_scalar('memory/cached', torch.cuda.memory_reserved(), epoch)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            epoch_loss = 0
            epoch_acc = 0
            epoch_auroc = 0
            iterations = 0
            batchNum = 0

            epoch_gtLabels = []
            epoch_predLabels = []

            for images, labels in tqdm(dataloaders[phase]):
                #print('loop', phase)

                images = images.to(device)
                labels = labels.to(device)

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    # zero the parameter gradients
                    optimizer.zero_grad()

                    outputs = model(images)
                    loss = criterion(outputs, labels)

                    # print(outputs)

                    #print('{} Batch {}/{}'.format(phase, batchNum, len(dataloaders[phase]) - 1))
                    batchNum += 1

                    if not math.isfinite(loss.item()):
                        print("Loss is {} during {}".format(loss.item(), phase))
                        if phase == 'train':
                            print('Quitting!')
                            quit()

                    #losses = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                    epoch_loss += loss.item()
                    preds = torch.max(outputs, 1)[1]
                    gt = labels.data

                    epoch_acc += torch.sum(preds == gt).double() / len(outputs)
                    epoch_gtLabels.append(gt.cpu())
                    epoch_predLabels.append(preds.cpu())

            all_epoch_gt = torch.cat(epoch_gtLabels)
            all_epoch_preds = torch.cat(epoch_predLabels)
            fpr, tpr, thresholds = roc_curve(y_true = all_epoch_gt, y_score = all_epoch_preds, pos_label = 1) #positive class is 1; negative class is 0
            epoch_auroc = auc(fpr, tpr)        

            lossloss = epoch_loss / len(dataloaders[phase])
            epoch_acc = epoch_acc / len(dataloaders[phase])

            if phase == 'train' and scheduler:
                scheduler.step()

            writer.add_scalar('loss/{}'.format(phase), lossloss, epoch)
            writer.add_scalar('accuracy/{}'.format(phase), epoch_acc, epoch)
            writer.add_scalar('auroc/{}'.format(phase), epoch_auroc, epoch)

            # if phase == 'train':
            #     for name, weight in model.named_parameters():
            #         writer.add_histogram(name, weight, epoch)
            #         writer.add_histogram(f'{name}.grad', weight.grad, epoch)

            # deep copy the model
            # if phase == 'val' and epoch_acc > best_acc:
            #     best_model_wts = copy.deepcopy(model.state_dict())
            if checkpoint_dir is not None and (epoch + 1) % 1 == 0 and phase == 'val':
                print('Checkpoint saving for epoch {:03d}.'.format(epoch))
                saveModelCheckpoint(epoch, 
                                    epoch_loss, 
                                    model, 
                                    optimizer, 
                                    scheduler, 
                                    os.path.join(checkpoint_dir, 'model_epoch_{:03d}.pth'.format(epoch)),
                                    criterion,
                                    model_type)
        time_elapsed = time.time() - epoch_since
        #print('Epoch completed in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))

    print()
    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))

    writer.flush()
    writer.close()

    # load best model weights
    # model.load_state_dict(best_model_wts)
    return model

