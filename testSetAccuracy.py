import torch
import os
from utils import *
import argparse
import random
from tqdm import tqdm
from torchvision.datasets import ImageFolder
from torchvision import transforms

from sklearn.metrics import roc_curve, auc, accuracy_score, confusion_matrix, average_precision_score
import yaml

parser = argparse.ArgumentParser(description='Test model accuracy for every model in a folder on a full dataset folder (or subset)')

#PyTorch Arguments
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--checkpoint_folder', type=str, required=True, help="pre-trained model file")
parser.add_argument('--dataset_root', type=str, default=os.path.join('.', 'ClassDataset'), help='directory path to ISIC segmentation dataset root')
parser.add_argument('--model_input_size', type=float, default=512, help="Amount by which to downscale dataset images.")
parser.add_argument('--num_images', type=int, default=0, help='Number of images randomly sampled to test against. Omit to test all images in the validation set')
parser.add_argument('--seed', type=int, default=456, help='random seed to use. Default=456')

#Parse Arguments
opt = parser.parse_args()

def getClassifierTransforms(imgSize):
    return transforms.Compose([
        # transforms.ToPILImage(),
        transforms.Resize((imgSize, imgSize)),
        #transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean_sums, std_sums)
    ])

device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')

#Make RNG deterministic
torch.manual_seed(opt.seed)
np.random.seed(opt.seed)
random.seed(opt.seed)
if torch.cuda.is_available():
    torch.cuda.manual_seed_all(opt.seed)

#===========================================
train_dataset = ImageFolder(os.path.join(opt.dataset_root), getClassifierTransforms(opt.model_input_size))
valSampleSize = opt.num_images if opt.num_images else len(train_dataset)
val_sampler = getSubSampler(1, random.choices(list(range(len(train_dataset))), k=valSampleSize ) )
valLoader = torch.utils.data.DataLoader(train_dataset, 
                                            batch_size=1, 
                                            shuffle=False, 
                                            num_workers=0,
                                            sampler=val_sampler)

#Load and prepare model ====================
class_list = [
    'benign',
    'malignant'
]

model_files = list(sorted(os.listdir(opt.checkpoint_folder)))

auroc_scores = []
acc_scores = []

def getPredictionMobileNet(classifierOutput):
    probs = torch.exp(classifierOutput)

    return probs.squeeze(0)[1].item()

def getPredictionEfficientNet(classifierOutput):
    probs = torch.softmax(classifierOutput, dim=1)

    return probs.squeeze(0)[1].item()

statsDict = {}
statsDict['inputDirectory'] = opt.dataset_root
statsDict['seed'] = opt.seed
statsDict['model_input_size'] = opt.model_input_size
statsDict['checkpointDirectory'] = opt.checkpoint_folder

for model_file in model_files:
    model_file_path = os.path.join(opt.checkpoint_folder, model_file)
    print(model_file_path)
    model = torch.load(model_file_path, map_location=device)

    model_type = 0

    if type(model) == dict:
        if 'full_model' in model.keys() and model['full_model'] is not None:
            model = model['full_model']
        elif 'model_state' in model.keys() and model['model_state'] is not None:
            state = model['model_state']
            if 'model_type' in model.keys() and model['model_type'] != 0:
                if model['model_type'] >= 1 and model['model_type'] < 8:
                    model_type = model['model_type']
                    model = getEfficientNetClassifier(num_classes=2, size='b{}'.format(model['model_type']-1), model_state=state)
                    opt.model_input_size = EfficientNet.get_image_size(f'efficientnet-b{model_type-1}')
                elif model['model_type'] >= 8:
                    model = getMobileNetV3Classifier(num_classes=2, model_state=state)
            else:
                model = getMobileNetV2Classifier(num_classes=2, model_state=state)

    train_dataset = ImageFolder(os.path.join(opt.dataset_root), getClassifierTransforms(opt.model_input_size))
    valLoader = torch.utils.data.DataLoader(train_dataset, 
                                            batch_size=1, 
                                            shuffle=False, 
                                            num_workers=0,
                                            sampler=val_sampler)

    model = model.to(device)
    model.eval()
    #===========================================

    predictionsList = []
    probsList = []
    gtList = []

    inc = 0
    for images, labels in tqdm(valLoader):
        classifierOutputs = model(images.to(device))
        labels = labels.to(device)

        if model_type == 0 or model_type >= 8:
            probs = getPredictionMobileNet(classifierOutputs)
        else:
            probs = getPredictionEfficientNet(classifierOutputs)

        preds = torch.max(classifierOutputs, 1)[1]
        predictionsList.append(preds.cpu())
        gtList.append(labels.data.cpu())
        probsList.append(probs)

    all_img_gt = torch.cat(gtList)
    all_img_preds = torch.cat(predictionsList)
    all_img_probs = probsList

    fpr, tpr, thresholds = roc_curve(y_true = all_img_gt, y_score = all_img_probs, pos_label = 1) #positive class is 1; negative class is 0
    auroc = auc(fpr, tpr)

    avg_precision = average_precision_score(y_true = all_img_gt, y_score = all_img_probs)
    accuracy = accuracy_score(y_true = all_img_gt, y_pred = all_img_preds)

    auroc_scores.append(auroc)
    acc_scores.append(accuracy)

    conf_matrix = confusion_matrix(y_true = all_img_gt, y_pred = all_img_preds)

    print(conf_matrix)
    print(f'AUROC: {auroc}, accuracy: {accuracy}, PR AUC: {avg_precision}')

    tn, fp = conf_matrix[0]
    fn, tp = conf_matrix[1]

    tn = float(tn)
    fp = float(fp)
    fn = float(fn)
    tp = float(tp)


    specificity = tn / (tn + fp)
    sensitivity = tp / (fn + tp)
    informedness = specificity + sensitivity - 1.0
    LRPlus = sensitivity / (1-specificity)
    LRMinus = (1-sensitivity) / specificity
    print(f'Specificity/TNR: {specificity}, FPR: {1-specificity}, PPV: {tp / (tp + fp)}, NPV: {tn / (tn + fn)}')
    print(f'Sensitivity/TPR: {sensitivity}, FNR: {1 - sensitivity}, Informedness: {informedness}')
    print(f'LR+: {LRPlus}, LR-: {LRMinus}, Diag odds ratio: {LRPlus/LRMinus}')
    print()

    statsDict[model_file] = {
        'conf_matrix': [[tn, fp], [fn, tp]],
        'auroc': float(auroc), 'accuracy': float(accuracy), 'auprc': float(avg_precision),
        'specificity/TNR': specificity, 'FPR': 1-specificity, 'PPV': tp/(tp+fp), 'NPV': tn/(tn+fn),
        'sensitivity/TPR': sensitivity, 'FNR': 1-sensitivity, 'informedness': informedness,
        'LR+': LRPlus, 'LR-': LRMinus, 'Diag odds ratio': (LRPlus/LRMinus)
    }

statsDict['model_input_size'] = opt.model_input_size
os.makedirs('stats', exist_ok=True)
statsFileName = f'testSet{getTimestamp()}.yml'
with open(os.path.join('stats', statsFileName), 'w') as wf:
    print(f'Writing out to stats/{statsFileName}.')
    yaml.dump(statsDict, wf)