

import os, random
import argparse

import numpy as np

from shutil import copy2
from shutil import rmtree

import cv2

import json
import ntpath

from utils import *

from tqdm import tqdm
from torchvision import transforms
from PIL import Image

parser = argparse.ArgumentParser(description='Construct a dataset usable for segmentation from ISIC')
parser.add_argument('--root', type=str, required=True, help='Root directory of Segmented ISIC dataset')
parser.add_argument('--output', type=str, required=True, help='Output directory path to create new segmentation dataset')
parser.add_argument('--show', action='store_true', help='show images?')
parser.add_argument('--min_percent', type=float, required=True, help='')
opt = parser.parse_args()

if opt.min_percent < 0 or opt.min_percent > 0.5:
    print('Error: --min_percent should be in the range [0, 0.5].')
    quit()

# Find paths to all images within dataset, split between benign and malignant. So like ClassDataset/ISICTrain/benign/... etc.

# Count the number of malignant vs. benign images. 

def findImagePaths(root, subFolder):
    basePath = os.path.join(root, subFolder)

    categoryFolder_files = os.listdir(basePath)

    categoryFolder_paths = [str(os.path.join(basePath, categoryFile)) for categoryFile in categoryFolder_files]
    imagePaths = sorted(categoryFolder_paths)

    return imagePaths

def determineSetBalance(benignPaths, malignantPaths):

    benignPaths_num = len(benignPaths)
    malignantPaths_num = len(malignantPaths)

    totalSize = benignPaths_num + malignantPaths_num
    benignProp = benignPaths_num / totalSize
    malignantProp = malignantPaths_num / totalSize

    print('Total Benign Examples: {}, Proportion: {}'.format(benignPaths_num, benignProp))
    print('Total Malignant Examples: {}, Proportion: {}'.format(malignantPaths_num, malignantProp))
    
    print('Final Malignant Proportion will be {}.'.format(opt.min_percent))
    response = input('Is this correct? (Y/n):')
    if response == 'Y' or response == 'y':
        newMalignantNum = int((opt.min_percent / (1 - opt.min_percent)) * benignPaths_num)
        newTotalSize = benignPaths_num + newMalignantNum
        newBenignProp = benignPaths_num / newTotalSize
        newMalignantProp = newMalignantNum / newTotalSize
        print()
        print('New Total Benign Examples: {}, Proportion: {}'.format(benignPaths_num, newBenignProp))
        print('New Total Malignant Examples: {}, Proportion: {}'.format(newMalignantNum, newMalignantProp))
        return benignPaths_num, newMalignantNum
    else:
        print('Exiting...')
        quit()

def getMalignancy(descPath):
    with open(descPath) as f:
        data = json.load(f)
        try:
            b = data['meta']['clinical']['benign_malignant']
            return b
        except KeyError:
            print('Not melanocytic.')
            return None

def cleanDataset(root):
    subPath = root  
    malignantPath = os.path.join(subPath, 'malignant')
    benignPath = os.path.join(subPath, 'benign')

    if os.path.exists(malignantPath):
        rmtree(malignantPath)
    
    if os.path.exists(benignPath):
        rmtree(benignPath)


def getMalignantTransforms():
    return transforms.Compose([
        #transforms.ColorJitter(brightness=0.3, contrast=0.1, saturation=0.1, hue=0.1),
        # transforms.RandomResizedCrop(size=512),
        transforms.RandomHorizontalFlip(),
        transforms.RandomVerticalFlip(),
        transforms.RandomAffine(degrees=10, translate=(0.05, 0.05), scale=(0.85, 1.0), shear=1),
        transforms.ToTensor()
    ])

def makeDataset(root, subFolder, paths):
    subPath = os.path.join(root, subFolder)
    os.makedirs(subPath, exist_ok=True)
    
    t = getMalignantTransforms()

    if subFolder == 'malignant':
        malignantPathCounts = { p: 0 for p in paths }

    #Write out the pair paths into a file
    for path in tqdm(paths, desc='Copying and modifying images'):
        img = Image.open(path).convert("RGB")
        img = np.array(t(img) * 255, dtype=np.uint8)

        img = np.moveaxis(img, 0, -1)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        if opt.show:
            cv2.imshow('m', img)
            cv2.waitKey()

        oldBaseName = ntpath.basename(path)
        oldFileEnding = oldBaseName[oldBaseName.rfind('.'):]

        if subFolder == 'malignant':
            newBaseName = oldBaseName[:oldBaseName.rfind('.')] + '_' + str(malignantPathCounts[path]) + oldFileEnding
        else:
            newBaseName = oldBaseName

        outputImagePath = os.path.join(subPath, newBaseName)

        if subFolder == 'malignant':
            malignantPathCounts[path] += 1
        
        # cv2.imwrite(outputImagePath, img)
        if outputImagePath.lower().endswith('jpeg') or outputImagePath.lower().endswith('jpg'):
            cv2.imwrite(outputImagePath, img, [int(cv2.IMWRITE_JPEG_QUALITY), 95])
        else:
            cv2.imwrite(outputImagePath, img)

def oversampleMalignant(malignantPaths, newPathsNum):
    newIndices = random.choices(list(range(len(malignantPaths))), k=newPathsNum )

    newMalignantPaths = [ malignantPaths[idx] for idx in newIndices ]
    newMalignantPaths = sorted(newMalignantPaths)
    return newMalignantPaths
    
print('Collecting image paths...')
benignPaths = findImagePaths(opt.root, 'benign')
malignantPaths = findImagePaths(opt.root, 'malignant')

benignPaths_num, malignantPaths_num = determineSetBalance(benignPaths, malignantPaths)

newMalignantPaths = oversampleMalignant(malignantPaths, malignantPaths_num)

print()
print('The following directories will be created and overwritten:')

print(opt.output)
print('\t> benign')
print('\t> malignant')

print('\nThis CANNOT BE UNDONE!')

response = input('\nAre you sure? (Y/n):')

if response.lower() != 'y':
    print('Exiting...')
    exit(0)

os.makedirs(opt.output, exist_ok=True)

print('Cleaning...')
cleanDataset(opt.output)

print('Constructing benign set...')
makeDataset(opt.output, 'benign', benignPaths)
print('Constructing malignant set...')
makeDataset(opt.output, 'malignant', newMalignantPaths)

