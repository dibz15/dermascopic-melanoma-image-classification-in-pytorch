import os
import argparse
import numpy as np
from shutil import rmtree
import cv2
import json
import ntpath
from utils import *
import torch
from tqdm import tqdm
import random

parser = argparse.ArgumentParser(description='Construct a dataset usable for segmentation from ISIC')
parser.add_argument('--root', type=str, default='./ISIC', help='Root directory of ISIC dataset.')
parser.add_argument('--percentVal', type=float, default=0.2, help="Proportion of total dataset that will become validation")
parser.add_argument('--percentTest', type=float, default=0.05, help='Proportion of total dataset that will become test data')
parser.add_argument('--output', type=str, default='./ClassDataset', help='Output directory path to create new segmentation dataset')
parser.add_argument('--model', type=str, help='Model file for segmentation')
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--show', action='store_true', help='show images?')
parser.add_argument('--mask', action='store_true', help='mask images?')
parser.add_argument('--crop', action='store_true', help='crop images?')
parser.add_argument('--num_images', type=int, default=-1, help='How many images to pull from the full dataset')
parser.add_argument('--model_input_size', type=int, default=-1, help='Limit input size to the segmentation model')
parser.add_argument('--skipnotfound', action='store_true', help='Skip if mole isn\'t found by segmentation model?')
parser.add_argument('--max_output_size', type=int, default=-1, help='Limit save output size')

opt = parser.parse_args()

if opt.mask and not opt.model:
    print('Must supply masking model if using masking.')
    exit(1)

if opt.percentVal + opt.percentTest >= 1.0:
    print('Validation and Test percentage shouldn\'t exceed the whole.')
    exit(1)

if opt.percentVal <= 0.0:
    print('Validation set size must be greater than 0.0')
    exit(1)

print('Loading Segmentation model...')

if opt.model:
    device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')
    model = torch.load(opt.model, map_location=device)

    if type(model) == dict:
        if 'full_model' in model.keys() and model['full_model'] is not None:
            model = model['full_model']
        elif 'model_state' in model.keys() and model['model_state'] is not None:
            if 'backbone_state' in model.keys():
                model = getModifiedMaskRCNN(backbone_state=model['backbone_state'], model_state=model['model_state'])
            else:
                model = getModifiedMaskRCNN(model_state=model['model_state'])

        model = model.to(device)
        model.eval()
    else:
        model = None
else:
    model = None

#Make RNG deterministic
torch.manual_seed(opt.seed)
np.random.seed(opt.seed)
random.seed(opt.seed)
if torch.cuda.is_available():
    torch.cuda.manual_seed_all(opt.seed)

# Create a list that pairs together the Segmentation (paths) with their images (paths)

# Separate the list into training and validation and training

# Create a new dataset folder, perhaps called SegDataset, with subfolders 
# ISICTrain, and ISICVal, and ISICTrain

# Copy the images using their paths into their new appropriate folders.
# Under each set folder should be also the folders Images and Segmentations.
# For example, ISICTrain would have ISICTrain/Images and ISICTrain/Segmentations

def findImagePaths(root, subFolder, blackList=None):
    basePath = os.path.join(root, subFolder)

    categoryFolder_files = set(os.listdir(basePath))

    if blackList is not None:
        foundMatches = categoryFolder_files & blackList
        # if len(foundMatches) > 0:
        #     print(foundMatches)
        categoryFolder_files = categoryFolder_files - foundMatches #Remove files that were matched with blacklist

    categoryFolder_paths = [str(os.path.join(basePath, categoryFile)) for categoryFile in categoryFolder_files]
    imagePaths = sorted(categoryFolder_paths)

    return imagePaths


def subsetPaths(pathPairs, percentVal, percentTest):
    assert(percentVal + percentTest <= 1.0)

    indexList = list(range(len(pathPairs)))
    indexList = np.array(indexList)
    np.random.shuffle(indexList)

    # Only use a subset of the dataset
    if opt.num_images > 0:
        indexList = indexList[0:opt.num_images]
        totalNum = len(indexList)
    else:
        totalNum = len(pathPairs)

    numVal = int(float(totalNum) * percentVal)
    numTest = int(float(totalNum)* percentTest)
    numTrain = totalNum - numVal - numTest

    trainPairs = []
    for index in indexList[0:numTrain]:
        trainPairs.append(pathPairs[index])

    valPairs = []
    for index in indexList[numTrain:numTrain+numVal]:
        valPairs.append(pathPairs[index])

    testPairs = []
    for index in indexList[numTrain+numVal:numTrain+numVal+numTest]:
        testPairs.append(pathPairs[index])

    return trainPairs, valPairs, testPairs

def getMalignancy(descPath):
    with open(descPath) as f:
        data = json.load(f)
        try:
            b = data['meta']['clinical']['benign_malignant']
            return b
        except KeyError:
            # print('Not melanocytic.')
            return None

def getMalignancyCounts(pairs, setName='Train'):
    benign = 0
    malignant = 0
    nonMelanocytic = 0
    for pair in tqdm(pairs, desc=f'Counting malignancies in ISIC{setName}.'):

        #print(pair[1])
        malignancy = getMalignancy(pair[1])
        if malignancy is None:
            nonMelanocytic += 1
            continue

        if malignancy == 'benign':
            benign += 1
        elif malignancy == 'malignant':
            malignant += 1
        else:
            nonMelanocytic += 1

    return benign, malignant, nonMelanocytic

print('Collecting image paths...')
imagePaths = findImagePaths(opt.root, 'Images')
print('Collecting mask paths...')
descriptionPaths = findImagePaths(opt.root, 'Descriptions')

print('Using mask paths to find valid image paths and pairing them up.')
pathPairs = [i for i in zip(imagePaths, descriptionPaths)]

print('Separating dataset into Train, Validation, and Test...')
print('\tTotal Valid Dataset Size:', len(pathPairs))
print('\tValidation Proportion:', opt.percentVal)
print('\tTest Proportion:', opt.percentTest)
trainPairs, valPairs, testPairs = subsetPaths(pathPairs, opt.percentVal, opt.percentTest)

# print()

# numBenignTrain, numMalignantTrain, numNonMelanocyticTrain = getMalignancyCounts(trainPairs, setName='Train')
# numBenignVal, numMalignantVal, numNonMelanocyticVal = getMalignancyCounts(valPairs, setName='Val')

# if len(testPairs) > 0:
#     numBenignTest, numMalignantTest, numNonMelanocyticTest = getMalignancyCounts(testPairs, setName='Test')

print()
print('\tFinal Train Size:', len(trainPairs))
# print(f'\t\tBenign: {numBenignTrain} + Malignant: {numMalignantTrain}, Non-melanocytic: {numNonMelanocyticTrain}')
print('\tFinal Val Size:', len(valPairs))
# print(f'\t\tBenign: {numBenignVal} + Malignant: {numMalignantVal}, Non-melanocytic: {numNonMelanocyticVal}')
print('\tFinal Test Size:', len(testPairs))
# print(f'\t\tBenign: {numBenignTest} + Malignant: {numMalignantTest}, Non-melanocytic: {numNonMelanocyticTest}')

def findMaskBoundingBoxes(mask):
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    bounding_boxes = [cv2.boundingRect(contour) for contour in contours]

    # Change bounding box format to x1,y1,x2,y2 and also sort largest -> smallest
    bounding_boxes = [[x, y, x+w, y+h] for x,y,w,h in sorted(bounding_boxes, key=lambda box: box[2], reverse=True)]
    scaleBoxes(bounding_boxes, scale=1.1)
    foundBoxes = [[x1,y1,x2,y2] for x1,y1,x2,y2 in bounding_boxes]

    return foundBoxes

def cropToBoundingBox(img, bounding_box, ow, oh):
    x1, y1, x2, y2 = bounding_box # Just crop with largest box
    x1 = max(0, min(x1, ow))
    y1 = max(0, min(y1, oh))
    x2 = max(0, min(x2, ow))
    y2 = max(0, min(y2, oh))
    if x2 - x1 <=2 or y2 - y1 <= 2:
        return None

    return img[int(y1):int(y2), int(x1):int(x2)]


def cleanDataset(root, subFolder):
    subPath = os.path.join(root, subFolder)    
    malignantPath = os.path.join(subPath, 'malignant')
    benignPath = os.path.join(subPath, 'benign')

    if os.path.exists(malignantPath):
        rmtree(malignantPath)
    
    if os.path.exists(benignPath):
        rmtree(benignPath)

def maskImage(model, image):
    if model is None:
        return None

    (oh, ow) = image.shape[:2]
    if opt.model_input_size > 0:
        proportion = opt.model_input_size / max(oh, ow)
        proportion = min(1.0, proportion) # Keep proportion under 1, we don't need to scale images up
        ih, iw = int(round(oh * proportion)), int(round(ow * proportion))
        resizedInputImage = cv2.resize(image, (iw, ih))
        modelInput = [getTestTransforms()(resizedInputImage).to(device)]
    else:
        proportion = 1.0
        modelInput = [getTestTransforms()(image).to(device)]

    pred = model(modelInput)
    sortedDict = sortPredictionsByClass(pred, 1)

    boxes = sortedDict[1]
    boxPicks = nonMaximumSuppression_pick(boxes, confThresh=0.2, iouThresh=0.0)
    boxes = boxes[boxPicks]
    if (len(boxPicks) > 0):
        #cvPutBoxes(img, boxes)

        if not opt.mask and not opt.crop:
            return image

        cleanedMasks = cleanupMasks(pred)
        maskPicks = cleanedMasks[0][boxPicks]

        if len(boxPicks) > 1:
            bestBoxIdx = np.argmax(boxes[:, 4], axis=0)
        else:
            bestBoxIdx = 0

        mask = maskPicks[bestBoxIdx] * 255

        mask[np.where(mask >= 127)] = 255
        mask[np.where(mask < 127)] = 0
        mask = np.array(mask, dtype=np.uint8)

        if proportion != 1.0:
            mask = cv2.resize(mask, (ow, oh), interpolation=cv2.INTER_NEAREST)

        if opt.mask:
            maskRGB = cv2.cvtColor(mask, cv2.COLOR_GRAY2RGB)
            maskedImg = cv2.bitwise_and(image, maskRGB)
        else:
            maskedImg = image

        if opt.crop:
            bounding_boxes = findMaskBoundingBoxes(mask)
            scaleBoxes(bounding_boxes, scale=1.15)
            if len(bounding_boxes):
                croppedImg = cropToBoundingBox(maskedImg, bounding_boxes[0], mask.shape[1], mask.shape[0])
                w, h = croppedImg.shape[1], croppedImg.shape[0]
                if w > (0.25 * h) and (w > 32 and h > 32):
                    maskedImg = croppedImg

        return maskedImg
    else:
        return None

def makeDataset(root, subFolder, pairs, model):
    subPath = os.path.join(root, subFolder)
    os.makedirs(subPath, exist_ok=True)
    
    malignantPath = os.path.join(subPath, 'malignant')
    benignPath = os.path.join(subPath, 'benign')

    os.makedirs(malignantPath, exist_ok=True)
    os.makedirs(benignPath, exist_ok=True)

    numNotMelanocytic = 0
    numNotFound = 0
    #Write out the pair paths into a file
    for pair in tqdm(pairs):

        #print(pair[1])
        malignancy = getMalignancy(pair[1])
        if malignancy is None:
            numNotMelanocytic += 1
            continue

        if malignancy == 'benign':
            outputImagePath = os.path.join(benignPath, ntpath.basename(pair[0]))
        elif malignancy == 'malignant':
            outputImagePath = os.path.join(malignantPath, ntpath.basename(pair[0]))
        else:
            numNotMelanocytic += 1

        #Read CV2 Image
        g_img = cv2.imread(pair[0])

        if opt.mask or opt.crop:
            maskedImage = maskImage(model, g_img)
            if maskedImage is None:
                # print('Mole not found')
                numNotFound += 1
                if opt.skipnotfound:
                    continue
                maskedImage = g_img
                # continue

            if opt.show:
                cv2.imshow('m', g_img)
                cv2.imshow('masked', maskedImage)
                cv2.waitKey()
        else:
            maskedImage = g_img

        #Scale our image so its max dimension is capped by max_output_size
        if opt.max_output_size > 0:
            (oh, ow) = maskedImage.shape[:2]
            proportion = opt.max_output_size / max(oh, ow)
            proportion = min(1.0, proportion) # Keep proportion under 1, we don't need to scale images up
            ih, iw = int(round(oh * proportion)), int(round(ow * proportion))
            maskedImage = cv2.resize(maskedImage, (iw, ih))

        if outputImagePath.lower().endswith('jpeg') or outputImagePath.lower().endswith('jpg'):
            cv2.imwrite(outputImagePath, maskedImage, [int(cv2.IMWRITE_JPEG_QUALITY), 95])
        else:
            cv2.imwrite(outputImagePath, maskedImage)

    print(f'Not Melanocytic: {numNotMelanocytic}, Not found: {numNotFound}')
    if not opt.skipnotfound:
        numNotFound = 0

    return len(pairs) - numNotMelanocytic - numNotFound

print('The following directories will be created and/or overwritten:')

print(opt.output)
print('\t> ISICTrain')
print('\t\t> benign')
print('\t\t> malignant')
print('\t> ISICVal')
print('\t\t> benign')
print('\t\t> malignant')

if len(testPairs) > 0:
    print('\t> ISICTest')
    print('\t\t> benign')
    print('\t\t> malignant')
    
print('\nThis CANNOT BE UNDONE!')

response = input('\nAre you sure? (Y\\n):')

if response.lower() != 'y':
    print('Exiting...')
    exit(0)

os.makedirs(opt.output, exist_ok=True)

print('Cleaning ISICTrain...')
cleanDataset(opt.output, 'ISICTrain')

print('Cleaning ISICVal')
cleanDataset(opt.output, 'ISICVal')

if len(testPairs) > 0:
    print('Cleaning ISICTest')
    cleanDataset(opt.output, 'ISICTest')

print('\nConstructing ISICTrain...')
trainFinalSize = makeDataset(opt.output, 'ISICTrain', trainPairs, model)

print('\nConstructing ISICVal...')
valFinalSize = makeDataset(opt.output, 'ISICVal', valPairs, model)

totalFinalSize = trainFinalSize + valFinalSize

if len(testPairs) > 0:
    print('\nConstructing ISICTest...')
    testFinalSize = makeDataset(opt.output, 'ISICTest', testPairs, model)
    totalFinalSize += testFinalSize

print('Final Output Sizes (not including skipped images):')
print(f'\t> ISICTrain: {trainFinalSize} ({trainFinalSize/totalFinalSize:.2f})')
print(f'\t> ISICVal: {valFinalSize} ({valFinalSize/totalFinalSize:.2f})')

if len(testPairs) > 0:
    print(f'\t> ISICTest: {testFinalSize} ({testFinalSize/totalFinalSize:.2f})')