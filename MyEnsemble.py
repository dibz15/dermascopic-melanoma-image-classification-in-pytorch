
import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F

# class MyEnsemble(nn.Module):

#     def __init__(self, num_classes, models: list):
#         super(MyEnsemble, self).__init__()

#         self.models = models
#         num_inputs = len(models)

#         self.output_gatherers = []

#         for _ in models:
#             self.output_gatherers.append(nn.Linear(2, 1))          

#         self.classifier = nn.Linear(num_inputs, num_classes)

#     def forward(self, x):
#         x_heads = []

#         #with torch.no_grad():
#         for idx, model in enumerate(self.models):
#             x_gathered = self.output_gatherers[idx](model(x.clone()))
#             x_heads.append(x_gathered)

#         x = torch.cat(x_heads, dim=1)

#         x = self.classifier(x)

#         return x

class MyEnsemble(nn.Module):

    def __init__(self, num_classes, models: list, device='cuda:0'):
        super(MyEnsemble, self).__init__()

        self.models = models
        num_inputs = len(models)
        self.fc_1 = nn.Linear(num_inputs * num_classes, num_classes)
        self.fc_2 = nn.Linear(num_inputs, num_classes)
        self._init_weights()

        self.total_weights = 0
        for model in models:
            self.total_weights += 1408
            model._fc = nn.Identity()

        self.classifier = nn.Linear(self.total_weights, num_classes)
        self.device = device


    def _init_weights(self):

        torch.nn.init.xavier_uniform_(self.fc_1.weight)
        self.fc_1.bias.data.fill_(0.01)

        torch.nn.init.xavier_uniform_(self.fc_2.weight)
        self.fc_2.bias.data.fill_(0.01)

    def forward(self, x):
        x_heads = []

        #with torch.no_grad():
        for model in self.models:
            x_heads.append(model(x.clone()))

        x = torch.cat(x_heads, dim=1)

        x = self.classifier(x)

        return x