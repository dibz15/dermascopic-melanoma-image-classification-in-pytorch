import os
import yaml


if __name__ == "__main__":
    allObjs = []
    for path in os.listdir():
        if '.yml' in path and 'filteredResults' not in path:
            with open(path, 'r') as f:
                fileObj = yaml.safe_load(f)
                allObjs.append(fileObj)


    finalResults = []
    # Get all epochs with sensitivity/TPR >= 0.79
    for singleFolderResults in allObjs:
        for key in singleFolderResults.keys():
            if '.pth' in key:
                singleModelResults = singleFolderResults[key]
                if singleModelResults['sensitivity/TPR'] >= 0.80 and singleModelResults['Diag odds ratio'] >= 120:
                    singleModelResults['checkpointDirectory'] = singleFolderResults['checkpointDirectory']
                    singleModelResults['inputDirectory'] = singleFolderResults['inputDirectory']
                    singleModelResults['seed'] = singleFolderResults['seed']
                    singleModelResults['model_input_size'] = singleFolderResults['model_input_size']
                    singleModelResults['model_file'] = key

                    finalResults.append(singleModelResults)

    finalResults = sorted(finalResults, key=lambda obj: obj['FPR'], reverse=True)

    with open('filteredResults.yml', 'w') as wf:
        yaml.dump(finalResults, wf)