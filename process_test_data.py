

import os
import argparse

import numpy as np
import pandas as pd

from shutil import copy2
from shutil import rmtree

import cv2

import json
import ntpath

from utils import *

import torch

from tqdm import tqdm


parser = argparse.ArgumentParser(description='Construct a dataset usable for segmentation from ISIC')
parser.add_argument('--root', type=str, default='./ISIC_Competition', help='Root directory of ISIC dataset.')
parser.add_argument('--output', type=str, default='./ISIC_Competition/test_processed_nomask', help='Output directory path to create new segmentation dataset')
parser.add_argument('--model', type=str, help='Model file for segmentation')
parser.add_argument('--max_output_size', type=int, default=-1, help='Limit save output size')
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--model_input_size', type=int, default=-1, help='Limit input size to the segmentation model')


opt = parser.parse_args()

if opt.model:
    device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')
    model = torch.load(opt.model, map_location=device)

    if type(model) == dict:
        if 'full_model' in model.keys() and model['full_model'] is not None:
            model = model['full_model']
        elif 'model_state' in model.keys() and model['model_state'] is not None:
            if 'backbone_state' in model.keys():
                model = getModifiedMaskRCNN(backbone_state=model['backbone_state'], model_state=model['model_state'])
            else:
                model = getModifiedMaskRCNN(model_state=model['model_state'])

        model = model.to(device)
        model.eval()
    else:
        model = None
else:
    model = None

def findImagePaths(root, subFolder, blackList=None):
    basePath = os.path.join(root, subFolder)

    categoryFolder_files = set(os.listdir(basePath))

    categoryFolder_paths = [str(os.path.join(basePath, categoryFile)) for categoryFile in categoryFolder_files]
    imagePaths = sorted(categoryFolder_paths)

    return imagePaths

print('Collecting image paths...')
imagePaths = findImagePaths(opt.root, 'test')

os.makedirs(opt.output, exist_ok=True)


def cleanDataset(root):
    if os.path.exists(root):
        rmtree(root)

# def makeDataset(root, paths):
#     subPath = root
#     os.makedirs(subPath, exist_ok=True)
    
#     #Write out the pair paths into a file
#     for path in tqdm(paths):

#         #print(pair[1])
#         basename = ntpath.basename(path)

#         outputImagePath = os.path.join(subPath, basename)

#         #Read CV2 Image
#         g_img = cv2.imread(path)

#         cv2.imwrite(outputImagePath, g_img)

def findMaskBoundingBoxes(mask):
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    bounding_boxes = [cv2.boundingRect(contour) for contour in contours]

    # Change bounding box format to x1,y1,x2,y2 and also sort largest -> smallest
    bounding_boxes = [[x, y, x+w, y+h] for x,y,w,h in sorted(bounding_boxes, key=lambda box: box[2], reverse=True)]
    scaleBoxes(bounding_boxes, scale=1.1)
    foundBoxes = [[x1,y1,x2,y2] for x1,y1,x2,y2 in bounding_boxes]

    return foundBoxes

def cropToBoundingBox(img, bounding_box, ow, oh):
    x1, y1, x2, y2 = bounding_box # Just crop with largest box
    x1 = max(0, min(x1, ow))
    y1 = max(0, min(y1, oh))
    x2 = max(0, min(x2, ow))
    y2 = max(0, min(y2, oh))
    if x2 - x1 <=2 or y2 - y1 <= 2:
        return None

    return img[int(y1):int(y2), int(x1):int(x2)]


def maskImage(model, image, bMask=False, bCrop=False):
    if model is None:
        return None

    (oh, ow) = image.shape[:2]
    if opt.model_input_size > 0:
        proportion = opt.model_input_size / max(oh, ow)
        proportion = min(1.0, proportion) # Keep proportion under 1, we don't need to scale images up
        ih, iw = int(round(oh * proportion)), int(round(ow * proportion))
        resizedInputImage = cv2.resize(image, (iw, ih))
        modelInput = [getTestTransforms()(resizedInputImage).to(device)]
    else:
        proportion = 1.0
        modelInput = [getTestTransforms()(image).to(device)]

    pred = model(modelInput)
    sortedDict = sortPredictionsByClass(pred, 1)

    boxes = sortedDict[1]
    boxPicks = nonMaximumSuppression_pick(boxes, confThresh=0.2, iouThresh=0.0)
    boxes = boxes[boxPicks]
    if (len(boxPicks) > 0):
        #cvPutBoxes(img, boxes)

        if not bMask and not bCrop:
            return image

        cleanedMasks = cleanupMasks(pred)
        maskPicks = cleanedMasks[0][boxPicks]

        if len(boxPicks) > 1:
            bestBoxIdx = np.argmax(boxes[:, 4], axis=0)
        else:
            bestBoxIdx = 0

        mask = maskPicks[bestBoxIdx] * 255

        mask[np.where(mask >= 127)] = 255
        mask[np.where(mask < 127)] = 0
        mask = np.array(mask, dtype=np.uint8)

        if proportion != 1.0:
            mask = cv2.resize(mask, (ow, oh), interpolation=cv2.INTER_NEAREST)

        if bMask:
            maskRGB = cv2.cvtColor(mask, cv2.COLOR_GRAY2RGB)
            maskedImg = cv2.bitwise_and(image, maskRGB)
        else:
            maskedImg = image

        if bCrop:
            bounding_boxes = findMaskBoundingBoxes(mask)
            scaleBoxes(bounding_boxes, scale=1.15)
            if len(bounding_boxes):
                croppedImg = cropToBoundingBox(maskedImg, bounding_boxes[0], mask.shape[1], mask.shape[0])
                w, h = croppedImg.shape[1], croppedImg.shape[0]
                if w > (0.25 * h) and (w > 32 and h > 32):
                    maskedImg = croppedImg

        return maskedImg
    else:
        return None

def makeDataset(root, paths, model):
    numNotFound = 0
    subPath = root
    os.makedirs(subPath, exist_ok=True)
    #Write out the pair paths into a file
    for path in tqdm(paths):
        #Read CV2 Image
        basename = ntpath.basename(path)
        outputImagePath = os.path.join(subPath, basename)

        #Read CV2 Image
        g_img = cv2.imread(path)

        maskedImage = maskImage(model, g_img, bCrop=True)
        if maskedImage is None:
            # print('Mole not found')
            numNotFound += 1
            maskedImage = g_img
            # continue

        #Scale our image so its max dimension is capped by max_output_size
        if opt.max_output_size > 0:
            (oh, ow) = maskedImage.shape[:2]
            proportion = opt.max_output_size / max(oh, ow)
            proportion = min(1.0, proportion) # Keep proportion under 1, we don't need to scale images up
            ih, iw = int(round(oh * proportion)), int(round(ow * proportion))
            maskedImage = cv2.resize(maskedImage, (iw, ih))

        if outputImagePath.lower().endswith('jpeg') or outputImagePath.lower().endswith('jpg'):
            cv2.imwrite(outputImagePath, maskedImage, [int(cv2.IMWRITE_JPEG_QUALITY), 95])
        else:
            cv2.imwrite(outputImagePath, maskedImage)

    print(f'Num not found: {numNotFound}')
    numNotFound = 0

    return len(paths) - numNotFound

print(f'Cleaning {opt.output}...')
cleanDataset(opt.output)

print('Making dataset.')
makeDataset(opt.output, imagePaths, model)