import torch
from utils import *
import cv2
import argparse
from torchvision import transforms
import random
import os

parser = argparse.ArgumentParser(description='FasterRCNN with MobileNetV2 on WIDER Face Dataset.')

#PyTorch Arguments
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--model_file', type=str, required=True, help="pre-trained model file")

#OpenCV arguments
parser.add_argument('--model_input_size', type=float, default=512, help="Amount by which to downscale dataset images.")
parser.add_argument('--input_file', type=str, required=True, help="image to run model on")
parser.add_argument('--output_file', type=str, help='Output file to write final video or image if required')
parser.add_argument('--in_type', type=str, default='img', help='Input type. \'v\' for video, \'img\' for image')
parser.add_argument('--seed', type=int, default=456, help='random seed to use. Default=456')
parser.add_argument('--verbose', action='store_true', help='verbose output?')


#Parse Arguments
opt = parser.parse_args()

#Make RNG deterministic
torch.manual_seed(opt.seed)
np.random.seed(opt.seed)
random.seed(opt.seed)
if torch.cuda.is_available():
    torch.cuda.manual_seed_all(opt.seed)


def getClassifierTransforms(imgSize):
    return transforms.Compose([
        transforms.ToPILImage(),
        transforms.Resize((imgSize, imgSize)),
        #transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean_sums, std_sums)
    ])

#Load and prepare model ====================
device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')
classifier_model_l = torch.load(opt.model_file, map_location=device)

model_type = 0

if type(classifier_model_l) == dict:
    if 'full_model' in classifier_model_l.keys() and classifier_model_l['full_model'] is not None:
        classifier_model = classifier_model_l['full_model']
    elif 'model_state' in classifier_model_l.keys() and classifier_model_l['model_state'] is not None:
        model_type = classifier_model_l['model_type']
        model_state = classifier_model_l['model_state']
        if model_type == 0:
            print('Creating new model template for MobileNet_V2')
            classifier_model = getMobileNetV2Classifier(num_classes=2, model_state=model_state)
            print(f'Model size: {sum(p.numel() for p in classifier_model.parameters())}')
        elif model_type >= 1 and model_type < 8:
            print('Creating new model template for EfficientNet-b{}'.format(model_type-1))
            classifier_model = getEfficientNetClassifier(num_classes=2, size='b{}'.format(model_type-1), model_state=model_state)
            print(f'Model size: {sum(p.numel() for p in classifier_model.parameters())}')
            opt.model_input_size = EfficientNet.get_image_size(f'efficientnet-b{model_type-1}')
        elif model_type >= 8:
            print('Creating model MobileNet V3')
            classifier_model = getMobileNetV3Classifier(num_classes=2, model_state=model_state)
            print(f'Model size: {sum(p.numel() for p in classifier_model.parameters())}')
        
print(f'Model input size: {opt.model_input_size}.')
classifier_model = classifier_model.to(device)
classifier_model.eval()
#===========================================

class_list = [
    'benign',
    'malignant'
]

def getPrediction(classifierOutput):
    # print(classifierOutput)
    #MobileNet
    if model_type  == 0 or model_type >= 8:
        probs = torch.exp(classifierOutput)
    #EfficientNet
    else:
        probs = torch.softmax(classifierOutput, dim=1)
    
    # print(probs)
    prob, pred = torch.max(probs, 1)
    pred_class = class_list[pred.item()]

    # print(prob)
    # print(pred)
    # print(pred_class)
    # quit()

    return prob.item(), pred.item(), pred_class

def readAndPredictImg(imgPath):    
    image = cv2.imread(imgPath)

    if image is None:
        print("Error opening file", imgPath, "as image.")
        exit(1)

    (oh, ow) = image.shape[:2]

    proportion = opt.model_input_size / max(oh, ow) 
    resizedImage = cv2.resize(image, (int(ow * proportion), int(oh * proportion)))

    imageConverted = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    classifierInput = getClassifierTransforms(opt.model_input_size)(imageConverted)
    classifierInput = classifierInput.unsqueeze(0).to(device)

    classifierOutput = classifier_model(classifierInput)

    prob, pred, pred_class = getPrediction(classifierOutput)
    return prob, pred, pred_class, resizedImage, image

#===========================================
#If source is image, open and predict
if opt.in_type == 'img' and opt.input_file:

    numBenignPred = 0
    numMalignPred = 0    
    if os.path.isdir(opt.input_file):
        outputLines = []
        for path in sorted(os.listdir(opt.input_file)):
            if os.path.isdir(os.path.join(opt.input_file, path)) or '.txt' in path:
                continue
            prob, pred, pred_class, _, image = readAndPredictImg(os.path.join(opt.input_file, path))
            if opt.verbose:
                print(f'Pred: {pred}, Prob: {prob:.3f}, Img: {path}')
            if pred == 0:
                numBenignPred += 1
            else:
                numMalignPred += 1

            text = '{}: Prediction - {} : {:.3f}'.format(path, pred_class, prob)
            print(text)
            outputLines.append(text + '\n')

        if opt.output_file:
            with open(opt.output_file, 'w') as wf:
                wf.writelines(outputLines)

        print(f'Predictions - Benign: {numBenignPred} | Malignant: {numMalignPred}.')

    else:
        prob, pred, pred_class, resizedImage, image = readAndPredictImg(opt.input_file)
        text = 'Prediction - {} : {:.3f}'.format(pred_class, prob)
        cv2.putText(resizedImage, 
                    text,
                    (10, 20),
                    cv2.FONT_HERSHEY_SIMPLEX, 
                    fontScale=0.5, color=(0, 255, 0), thickness=2)

        cv2.putText(image, 
                text,
                (40, 80),
                cv2.FONT_HERSHEY_SIMPLEX, 
                fontScale=1.5, color=(0, 255, 0), thickness=6)

        print(text)
        cv2.imshow('Image', resizedImage)
        cv2.waitKey()

        if opt.output_file:
            if opt.output_file.endswith('jpeg') or opt.output_file.endswith('jpg'):
                cv2.imwrite(opt.output_file, image, [int(cv2.IMWRITE_JPEG_QUALITY), 95])
            else:
                cv2.imwrite(opt.output_file, image)
