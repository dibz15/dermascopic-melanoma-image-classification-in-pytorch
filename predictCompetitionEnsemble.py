import torch

from PIL import Image
import math, time, copy, os

from utils import *
from torchvision.models.detection.faster_rcnn import FasterRCNN

import cv2
import numpy as np

import argparse
import matplotlib.pyplot as plt

from torchvision import transforms
import ntpath
from tqdm import tqdm

from ensemble_tools import *

parser = argparse.ArgumentParser(description='')

#PyTorch Arguments
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--model_file', type=str, required=True, help="pre-trained model file")

#OpenCV arguments
parser.add_argument('--model_input_size', type=float, default=512, help="Amount by which to downscale dataset images.")
parser.add_argument('--test_folder', type=str, required=True, help="Folder to run model on")
parser.add_argument('--output_dir', type=str, default='competition_out', help='Output folder to write final test .csv results')

#Parse Arguments
opt = parser.parse_args()

def getClassifierTransforms():
    return transforms.Compose([
        transforms.ToPILImage(),
        transforms.Resize((opt.model_input_size, opt.model_input_size)),
        #transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean_sums, std_sums)
    ])

#Load and prepare model ====================
device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')
classifier_model_l = torch.load(opt.model_file, map_location=device)

if type(classifier_model_l) == dict:
    if 'model_type' not in classifier_model_l:
        print('Error: wrong model type')
        exit(1)

    opt.model_type = classifier_model_l['model_type']

    _, _, classifier_model, ensemble_types = loadEnsembleFromCheckpoint(classifier_model_l, None, None)


classifier_model = classifier_model.to(device)
classifier_model.eval()

opt.model_input_size = EfficientNet.get_image_size('efficientnet-b{}'.format(opt.model_type - 1))
#===========================================

class_list = [
    'benign',
    'malignant'
]


def getTestPaths(folder):
    basePath = folder
    testImages = list(sorted(os.listdir(basePath)))
    return testImages

def getPredictionEfficientNet(classifierOutput):
    probs = torch.softmax(classifierOutput, dim=1)
    prob, _ = torch.max(probs, 1)
    _, pred = torch.max(classifierOutput, 1)

    return probs.squeeze(0)[1].item()

#===========================================
#If source is image, open and predict

rows = []

for image_name in tqdm(getTestPaths(opt.test_folder)):
    input_path = os.path.join(opt.test_folder, image_name)

    image = cv2.imread(input_path)

    if image is None:
        print("Error opening file", input_path, "as image.")
        exit(1)

    (oh, ow) = image.shape[:2]

    proportion = opt.model_input_size / max(oh, ow) 

    resizedImage = cv2.resize(image, (int(ow * proportion), int(oh * proportion)))

    classifierInput = getClassifierTransforms()(resizedImage).unsqueeze(0).to(device)
    classifierOutput = classifier_model(classifierInput)

    probability = getPredictionEfficientNet(classifierOutput)

    #print('File: {} - {}'.format(input_path, probability))
    basename = ntpath.basename(input_path)
    rows.append([basename.split('.')[0], probability])

os.makedirs(opt.output_dir, exist_ok=True)
output_path = os.path.join(opt.output_dir, 'test{}.csv'.format(getTimestamp()))

print('Writing file', output_path)
with open(output_path, 'w') as f:
    f.write('image_name,target\n')
    for row in rows:
        f.write('{},{}\n'.format(row[0], str(row[1])))