import torch
import os
from utils import *
import cv2
import argparse
from torchvision import transforms
import ntpath
from tqdm import tqdm
import random

parser = argparse.ArgumentParser(description='')

#PyTorch Arguments
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--model', type=str, help="pre-trained model file")
parser.add_argument('--model_benign', type=str, help="pre-trained model file better at benign examples")
parser.add_argument('--model_malignant', type=str, help="pre-trained model file better at malignant examples")
parser.add_argument('--test_folder', type=str, default=os.path.join('.', 'ClassDataset'), help='directory path to ISIC segmentation dataset root')
parser.add_argument('--model_input_size', type=float, default=512, help="Amount by which to downscale dataset images.")
parser.add_argument('--seed', type=int, default=456, help='random seed to use. Default=456')
parser.add_argument('--output_dir', type=str, default=os.path.join('.', 'competition_out'), help='directory path to ISIC segmentation dataset root')


#Parse Arguments
opt = parser.parse_args()

if not opt.model:
    if not opt.model_benign and not opt.model_malignant:
        print('Must supply either --model or both --model_benign and --model_malignant')
        exit(1)
elif not opt.model_benign and not opt.model_malignant:
    if not opt.model:
        print('Must supply either --model or both --model_benign and --model_malignant')
        exit(1)

def getClassifierTransforms(imgSize):
    return transforms.Compose([
        transforms.ToPILImage(),
        transforms.Resize((imgSize, imgSize)),
        #transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean_sums, std_sums)
    ])

device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')

#===========================================

#Make RNG deterministic
torch.manual_seed(opt.seed)
np.random.seed(opt.seed)
random.seed(opt.seed)
if torch.cuda.is_available():
    torch.cuda.manual_seed_all(opt.seed)

#Load and prepare model ====================
class_list = [
    'benign',
    'malignant'
]


#===========================================

def getTestPaths(folder):
    basePath = folder
    testImages = list(sorted(os.listdir(basePath)))
    return testImages


def getPredictionMobileNet(classifierOutput):
    probs = torch.exp(classifierOutput)

    return probs.squeeze(0)[1].item()

def getPredictionEfficientNet(classifierOutput):
    probs = torch.softmax(classifierOutput, dim=1)

    return probs.squeeze(0)[1].item()

def loadInModel(model_path):
    model_file_path = model_path
    print('Loading', model_path)
    model = torch.load(model_file_path, map_location=device)

    imgSize = opt.model_input_size

    if type(model) == dict:
        if 'model_state' in model.keys() and model['model_state'] is not None:
            state = model['model_state']
            if 'model_type' in model.keys() and model['model_type'] != 0:
                if model['model_type'] >= 1 and model['model_type'] < 8:
                    model_type = model['model_type']
                    model = getEfficientNetClassifier(num_classes=2, size='b{}'.format(model['model_type']-1), model_state=state)
                    imgSize = EfficientNet.get_image_size(f'efficientnet-b{model_type-1}')
                elif model['model_type'] >= 8:
                    model_type = model['model_type']
                    model = getMobileNetV3Classifier(num_classes=2, model_state=state)
            else:
                model_type = model['model_type']
                model = getMobileNetV2Classifier(num_classes=2, model_state=state)
        else:
            print('Model couldn\'t be loaded correctly.')
            exit(1)
    else:
        print('Model couldn\'t be loaded correctly.')
        exit(1)

    model = model.to(device)
    model.eval()

    return model, imgSize, model_type

def testSingleModel(model_file):
    model,imgSize, modelType = loadInModel(model_file)

    rows = []
    for image_name in tqdm(getTestPaths(opt.test_folder)):
        input_path = os.path.join(opt.test_folder, image_name)

        image = cv2.imread(input_path)

        if image is None:
            print("Error opening file", input_path, "as image.")
            exit(1)

        imageConverted = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        classifierInput = getClassifierTransforms(imgSize)(imageConverted).unsqueeze(0).to(device)
        classifierOutput = model(classifierInput)

        if modelType == 0 or modelType == 8:
            probability = getPredictionMobileNet(classifierOutput)
        else:
            probability = getPredictionEfficientNet(classifierOutput)

        #print('File: {} - {}'.format(input_path, probability))
        basename = ntpath.basename(input_path)
        rows.append([basename.split('.')[0], probability])

    return rows
    

def testJointModel(model_file_benign, model_file_malignant):
    model_benign, imgSize1, modelType1 = loadInModel(model_file_benign)
    model_malignant, imgSize2, modelType2 = loadInModel(model_file_malignant)

    imgSize = min(imgSize1, imgSize2)
    rows = []
    for image_name in tqdm(getTestPaths(opt.test_folder)):
        input_path = os.path.join(opt.test_folder, image_name)

        image = cv2.imread(input_path)

        if image is None:
            print("Error opening file", input_path, "as image.")
            exit(1)

        imageConverted = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        classifierInput = getClassifierTransforms(imgSize)(imageConverted).unsqueeze(0).to(device)
        classifierOutput = model_malignant(classifierInput)

        if modelType2 == 0 or modelType2 == 8:
            probability = getPredictionMobileNet(classifierOutput)
        else:
            probability = getPredictionEfficientNet(classifierOutput)

        _, pred = torch.max(classifierOutput, 1)
        prediction = pred.item()

        if prediction == 1: #If benign model thinks it's benign, we trust it
            pass
        else:
            #If benign model thinks that it's not benign, then let's run it through the malignant model and see what it thinks
            classifierOutput = model_benign(classifierInput)
            if modelType1 == 0 or modelType1 == 8:
                probability = getPredictionMobileNet(classifierOutput)
            else:
                probability = getPredictionEfficientNet(classifierOutput)

        #print('File: {} - {}'.format(input_path, probability))
        basename = ntpath.basename(input_path)
        rows.append([basename.split('.')[0], probability])

    return rows


if opt.model:
    rows = testSingleModel(opt.model)
else:
    rows = testJointModel(opt.model_benign, opt.model_malignant)

os.makedirs(opt.output_dir, exist_ok=True)
output_path = os.path.join(opt.output_dir, 'test{}.csv'.format(getTimestamp()))

print('Writing file', output_path)
with open(output_path, 'w') as f:
    f.write('image_name,target\n')
    for row in rows:
        f.write('{},{}\n'.format(row[0], str(row[1])))
