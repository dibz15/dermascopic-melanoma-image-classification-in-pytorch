import torch
import os
from utils import *
import numpy as np
import argparse
import matplotlib.pyplot as plt
from tqdm import tqdm
from torchvision.datasets import ImageFolder
from torchvision import transforms
from sklearn.metrics import average_precision_score, precision_recall_curve, roc_curve, auc, accuracy_score, confusion_matrix, RocCurveDisplay, PrecisionRecallDisplay
import seaborn as sn
import pandas as pd
import random

parser = argparse.ArgumentParser(description='MobileNetV2 Classifier on segmented ISIC dataset.')

#PyTorch Arguments
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--model', type=str, help="pre-trained model file")
parser.add_argument('--model_benign', type=str, help="pre-trained model file better at benign examples")
parser.add_argument('--model_malignant', type=str, help="pre-trained model file better at malignant examples")
parser.add_argument('--dataset_root', type=str, default=os.path.join('.', 'ClassDataset'), help='directory path to ISIC segmentation dataset root')
parser.add_argument('--model_input_size', type=float, default=512, help="Amount by which to downscale dataset images.")
parser.add_argument('--seed', type=int, default=456, help='random seed to use. Default=456')
parser.add_argument('--show_roc', action='store_true', help='show ROC curve')
parser.add_argument('--show_pr', action='store_true', help='show PR curve')


#Parse Arguments
opt = parser.parse_args()

if not opt.model:
    if not opt.model_benign and not opt.model_malignant:
        print('Must supply either --model or both --model_benign and --model_malignant')
        exit(1)
elif not opt.model_benign and not opt.model_malignant:
    if not opt.model:
        print('Must supply either --model or both --model_benign and --model_malignant')
        exit(1)

def getClassifierTransforms(imgSize):
    return transforms.Compose([
        # transforms.ToPILImage(),
        transforms.Resize((imgSize, imgSize)),
        #transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean_sums, std_sums)
    ])

device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')

#===========================================

#Make RNG deterministic
torch.manual_seed(opt.seed)
np.random.seed(opt.seed)
random.seed(opt.seed)
if torch.cuda.is_available():
    torch.cuda.manual_seed_all(opt.seed)

#Load and prepare model ====================
class_list = [
    'benign',
    'malignant'
]


def loadInModel(model_path):
    model_file_path = model_path
    print('Loading', model_path)
    model = torch.load(model_file_path, map_location=device)

    imgSize = opt.model_input_size

    if type(model) == dict:
        if 'model_state' in model.keys() and model['model_state'] is not None:
            state = model['model_state']
            if 'model_type' in model.keys() and model['model_type'] != 0:
                if model['model_type'] >= 1 and model['model_type'] < 8:
                    model_type = model['model_type']
                    model = getEfficientNetClassifier(num_classes=2, size='b{}'.format(model['model_type']-1), model_state=state)
                    imgSize = EfficientNet.get_image_size(f'efficientnet-b{model_type-1}')
                elif model['model_type'] >= 8:
                    model_type = model['model_type']
                    model = getMobileNetV3Classifier(num_classes=2, model_state=state)
            else:
                model_type = model['model_type']
                model = getMobileNetV2Classifier(num_classes=2, model_state=state)
        else:
            print('Model couldn\'t be loaded correctly.')
            exit(1)
    else:
        print('Model couldn\'t be loaded correctly.')
        exit(1)

    model = model.to(device)
    model.eval()

    return model, imgSize, model_type

def getPredictionMobileNet(classifierOutput):
    probs = torch.exp(classifierOutput)

    return probs.squeeze(0)[1].item()

def getPredictionEfficientNet(classifierOutput):
    probs = torch.softmax(classifierOutput, dim=1)

    return probs.squeeze(0)[1].item()

def testSingleModel(model_file):
    model,imgSize, modelType = loadInModel(model_file)
    train_dataset = ImageFolder(os.path.join(opt.dataset_root), getClassifierTransforms(imgSize))

    predictions = []
    gt = []
    probabilities = []
    for image, label in tqdm(train_dataset):
        unsqueezedImg = image.unsqueeze(0).to(device)
        classifierOutput = model(unsqueezedImg)

        if modelType == 0 or modelType == 8:
            probability = getPredictionMobileNet(classifierOutput)
        else:
            probability = getPredictionEfficientNet(classifierOutput)

        probabilities.append(probability)
        gt.append(float(label))

        _, pred = torch.max(classifierOutput, 1)
        predictions.append(pred.item())

    return predictions, probabilities, gt
    

def testJointModel(model_file_benign, model_file_malignant):
    model_benign, imgSize1, modelType1 = loadInModel(model_file_benign)
    model_malignant, imgSize2, modelType2 = loadInModel(model_file_malignant)

    imgSize = min(imgSize1, imgSize2)

    train_dataset = ImageFolder(os.path.join(opt.dataset_root), getClassifierTransforms(imgSize))

    predictions = []
    gt = []
    probabilities = []
    for image, label in tqdm(train_dataset):
        classifierOutput = model_malignant(image.unsqueeze(0).to(device))
        _, pred = torch.max(classifierOutput, 1)
        prediction = pred.item()

        if modelType2 == 0 or modelType2 == 8:
            probability = getPredictionMobileNet(classifierOutput)
        else:
            probability = getPredictionEfficientNet(classifierOutput)

        if prediction == 1: #If benign model thinks it's benign, we trust it
            pass
        else:
            #If benign model thinks that it's not benign, then let's run it through the malignant model and see what it thinks
            classifierOutput = model_benign(image.unsqueeze(0).to(device))
            _, pred = torch.max(classifierOutput, 1)
            prediction = pred.item()

            if modelType1 == 0 or modelType1 == 8:
                probability = getPredictionMobileNet(classifierOutput)
            else:
                probability = getPredictionEfficientNet(classifierOutput)

        predictions.append(prediction)
        gt.append(float(label))
        probabilities.append(probability)

    return predictions, probabilities, gt


if opt.model:
    predictions, probabilities, gt = testSingleModel(opt.model)
else:
    predictions, probabilities, gt = testJointModel(opt.model_benign, opt.model_malignant)

fpr, tpr, thresholds = roc_curve(y_true = gt, y_score = probabilities, pos_label = 1) #positive class is 1; negative class is 0
# print(fpr)
# print(tpr)
# print(thresholds)
auroc = auc(fpr, tpr)

avg_precision = average_precision_score(y_true = gt, y_score = probabilities)
accuracy = accuracy_score(y_true = gt, y_pred = predictions)
conf_matrix = confusion_matrix(y_true = gt, y_pred = predictions)

print(conf_matrix)
print('AUROC:', auroc)
print('Accuracy:', accuracy)
print(f'PR AUC: {avg_precision}')

tn, fp = conf_matrix[0]
fn, tp = conf_matrix[1]

specificity = tn / (tn + fp)
sensitivity = tp / (fn + tp)
informedness = specificity + sensitivity - 1.0
LRPlus = sensitivity / (1-specificity)
LRMinus = (1-sensitivity) / specificity
print(f'Specificity/TNR: {specificity}, FPR: {1-specificity}, PPV: {tp / (tp + fp)}, NPV: {tn / (tn + fn)}')
print(f'Sensitivity/TPR: {sensitivity}, FNR: {1 - sensitivity}, Informedness: {informedness}')
print(f'LR+: {LRPlus}, LR-: {LRMinus}, Diag odds ratio: {LRPlus/LRMinus}')
print()

top_sum = float(conf_matrix[0][0] + conf_matrix[0][1])
bottom_sum = float(conf_matrix[1][0] + conf_matrix[1][1])

new_matrix = []
new_matrix.append([float(conf_matrix[0][0]) / top_sum, float(conf_matrix[0][1]) / top_sum])
new_matrix.append([float(conf_matrix[1][0]) / bottom_sum, float(conf_matrix[1][1]) / bottom_sum])

df_cm = pd.DataFrame(new_matrix, class_list, class_list)
# plt.figure(figsize=(10,7))
sn.set(font_scale=1.4) # for label size
sn.heatmap(df_cm, annot=True, annot_kws={"size": 16}) # font size

plt.show()

if opt.show_roc:
    RocCurveDisplay.from_predictions(y_true = gt, y_pred = probabilities)
    plt.show()

if opt.show_pr:
    PrecisionRecallDisplay.from_predictions(y_true = gt, y_pred = probabilities)
    plt.show()