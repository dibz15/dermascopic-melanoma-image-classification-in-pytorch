

import os, random
import argparse

import numpy as np

from shutil import copy2
from shutil import rmtree

import cv2

import json
import ntpath

from utils import *

import torch

from tqdm import tqdm
from torchvision import transforms
from PIL import Image
from collections import Counter

from skimage.util import random_noise

parser = argparse.ArgumentParser(description='Construct an augmented ISIC dataset')
parser.add_argument('--root', type=str, required=True, help='Root directory of Augment Segmented ISIC dataset')
parser.add_argument('--output', type=str, required=True, help='Output directory path to create new segmentation dataset')
parser.add_argument('--show', action='store_true', help='show images?')
opt = parser.parse_args()


# Find paths to all images within dataset, split between benign and malignant. So like ClassDataset/ISICTrain/benign/... etc.

# Count the number of malignant vs. benign images. 

def findImagePaths(root, subFolder):
    basePath = os.path.join(root, subFolder)

    categoryFolder_files = os.listdir(basePath)

    categoryFolder_paths = [str(os.path.join(basePath, categoryFile)) for categoryFile in categoryFolder_files]
    imagePaths = sorted(categoryFolder_paths)

    return imagePaths


def getMalignancy(descPath):
    with open(descPath) as f:
        data = json.load(f)
        try:
            b = data['meta']['clinical']['benign_malignant']
            return b
        except KeyError:
            print('Not melanocytic.')
            return None

def cleanDataset(root):
    subPath = root  
    malignantPath = os.path.join(subPath, 'malignant')
    benignPath = os.path.join(subPath, 'benign')

    if os.path.exists(malignantPath):
        rmtree(malignantPath)
    
    if os.path.exists(benignPath):
        rmtree(benignPath)


def saveImage(img, path, subPath, num):
    img = np.array(img * 255, dtype=np.uint8)

    img = np.moveaxis(img, 0, -1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    if opt.show:
        cv2.imshow('m', img)
        cv2.waitKey()

    oldBaseName = ntpath.basename(path)
    oldFileEnding = oldBaseName[oldBaseName.rfind('.'):]
    newBaseName = oldBaseName[:oldBaseName.rfind('.')] + '_' + str(num) + oldFileEnding

    outputImagePath = os.path.join(subPath, newBaseName)

    cv2.imwrite(outputImagePath, img)

def makeDataset(root, subFolder, paths):
    subPath = os.path.join(root, subFolder)
    os.makedirs(subPath, exist_ok=True)
    

    #Write out the pair paths into a file
    for path in tqdm(paths):

        if subFolder == 'benign':
            img = Image.open(path).convert("RGB")

            t = transforms.Compose([
                transforms.ColorJitter(brightness=0.1, contrast=0.05, saturation=0.05, hue=0.01),
                transforms.ToTensor()
            ])
            imgSave = t(img)
            saveImage(imgSave, path, subPath, 0)

            # t = transforms.Compose([
            #     transforms.ColorJitter(brightness=0.4, contrast=0.1, saturation=0.1, hue=0.01),
            #     transforms.RandomHorizontalFlip(),
            #     transforms.RandomVerticalFlip(),
            #     transforms.RandomAffine(degrees=45, translate=(0.2, 0.2), scale=(0.9, 1.0), shear=1),
            #     transforms.ToTensor()
            # ])
            # imgSave = t(img)
            # imgSave = torch.tensor(
            #     random_noise(imgSave, mode='gaussian', mean=0, var=0.005, clip=True)
            # )
            # saveImage(imgSave, path, subPath, 1)

        else:
            img = Image.open(path).convert("RGB")

            #Normal
            t = transforms.Compose([
                transforms.ColorJitter(brightness=0.1, contrast=0.05, saturation=0.05, hue=0.01),
                transforms.ToTensor()
            ])
            imgSave = t(img)
            saveImage(imgSave, path, subPath, 0)

            # #Normal with noise
            # imgSave = torch.tensor(
            #     random_noise(imgSave, mode='gaussian', mean=0, var=0.005, clip=True)
            # )
            # saveImage(imgSave, path, subPath, 1)

            #Flipped horizontally
            t = transforms.Compose([
                transforms.ColorJitter(brightness=0.1, contrast=0.05, saturation=0.05, hue=0.01),
                transforms.RandomHorizontalFlip(p=1.0),
                #transforms.RandomAffine(degrees=45, translate=(0.2, 0.2), scale=(0.6, 1.0), shear=1),
                transforms.ToTensor()
            ])
            imgSave = t(img)
            saveImage(imgSave, path, subPath, 2)

            # #Flipped horizontally with noise
            # imgSave = t(img)
            # imgSave = torch.tensor(
            #     random_noise(imgSave, mode='gaussian', mean=0, var=0.005, clip=True)
            # )
            # saveImage(imgSave, path, subPath, 3)

            #Flipped vertically
            t = transforms.Compose([
                transforms.ColorJitter(brightness=0.1, contrast=0.05, saturation=0.05, hue=0.01),
                transforms.RandomVerticalFlip(p=1.0),
                #transforms.RandomAffine(degrees=45, translate=(0.2, 0.2), scale=(0.6, 1.0), shear=1),
                transforms.ToTensor()
            ])
            imgSave = t(img)
            saveImage(imgSave, path, subPath, 4)

            # #Flipped vertically with noise
            # imgSave = t(img)
            # imgSave = torch.tensor(
            #     random_noise(imgSave, mode='gaussian', mean=0, var=0.005, clip=True)
            # )
            # saveImage(imgSave, path, subPath, 5)            

            t = transforms.Compose([
                transforms.ColorJitter(brightness=0.1, contrast=0.05, saturation=0.05, hue=0.01),
                transforms.RandomHorizontalFlip(p=1.0),
                transforms.RandomVerticalFlip(p=1.0),
                #transforms.RandomAffine(degrees=45, translate=(0.2, 0.2), scale=(0.6, 1.0), shear=1),
                transforms.ToTensor()
            ])
            imgSave = t(img)
            saveImage(imgSave, path, subPath, 6)

            # imgSave = t(img)
            # imgSave = torch.tensor(
            #     random_noise(imgSave, mode='gaussian', mean=0, var=0.005, clip=True)
            # )
            # saveImage(imgSave, path, subPath, 7)

    
print('Collecting image paths...')
benignPaths = findImagePaths(opt.root, 'benign')
malignantPaths = findImagePaths(opt.root, 'malignant')

print('Set balance will be as follows:')
print('\tBenign: {}'.format(len(benignPaths)))
print('\tMalignant: {}'.format(len(malignantPaths) * 4))

os.makedirs(opt.output, exist_ok=True)

print('The following directories will be created and overwritten:')

print(opt.output)
print('\t> benign')
print('\t> malignant')

print('\nThis CANNOT BE UNDONE!')

response = input('\nAre you sure? (Y/n):')

if response.lower() != 'y':
    print('Exiting...')
    exit(0)

print('Cleaning...')
cleanDataset(opt.output)


print('Constructing benign set...')
makeDataset(opt.output, 'benign', benignPaths)
print('Constructing malignant set...')
makeDataset(opt.output, 'malignant', malignantPaths)

