
import numpy as np
import torchvision
import torch
from torch import nn
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection.faster_rcnn import FasterRCNN
from torchvision.models.detection.rpn import AnchorGenerator
from torch.utils.data import SubsetRandomSampler

from torchvision import transforms

from PIL import Image
import math, time, copy, os, random
import cv2
import matplotlib.pyplot as plt

from utils import *

from torch.utils.tensorboard import SummaryWriter 

import argparse

from torchvision.datasets import ImageFolder

from sklearn.metrics import roc_curve, auc

from tqdm import tqdm

from efficientnet_pytorch import EfficientNet

from model_trainer import train_model, getTrainingTransforms, getValidationTransforms, imshow

from MyEnsemble import MyEnsemble


from ensemble_tools import *

parser = argparse.ArgumentParser(description='MobileNetV2 on ISIC Dataset.')
parser.add_argument('--image_size', type=float, default=512, help="Size to crop/scale all input images")
parser.add_argument('--batchSize', type=int, default=15, help='training batch size')
parser.add_argument('--valBatchSize', type=int, default=15, help='validation batch size')
parser.add_argument('--epochs', type=int, default=50, help='number of epochs to train for')
parser.add_argument('--lr', type=float, default=0.0005, help='Learning Rate. Default=0.01')
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--threads', type=int, default=4, help='number of threads for data loader to use')
parser.add_argument('--seed', type=int, default=456, help='random seed to use. Default=123')
parser.add_argument('--checkpoint_dir', type=str, default=os.path.join('.', 'checkpoints_ensemble'), help="directory to save checkpoints into")
parser.add_argument('--model_save_dir', type=str, default=os.path.join('.', 'saved_models_ensemble'), help="directory to save final models into")
parser.add_argument('--dataset_root', type=str, default=os.path.join('.', 'dataset'), help='directory path to dataset root')
parser.add_argument('--weight_decay', type=float, default=0.2, help='Amount of weight decay for AdamW optimizer.')

# parser.add_argument('--train_num_images', type=int, default=-1, help='Number of training images to use from the set. Default is to use the whole dataset.')
# parser.add_argument('--val_num_images', type=int, default=-1, help='Number of validation images to use from dataset. Default is to use the whole dataset.')

parser.add_argument('--load_checkpoint', type=str, help="model file to load if resuming training")
parser.add_argument('--load_model', type=str, help="model file to load if training from custom pre-trained model")

opt = parser.parse_args()

os.makedirs(opt.model_save_dir, exist_ok=True)
checkpoint_dir = os.path.join(opt.checkpoint_dir, 'run{}'.format(getTimestamp()))
os.makedirs(checkpoint_dir, exist_ok=True)

#Make RNG deterministic
#torch.manual_seed(opt.seed)
#np.random.seed(opt.seed)

device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')
lr = opt.lr
num_epochs=opt.epochs


startEpoch = 0



print()
if opt.load_model is not None:
    print('Loading previously trained model', opt.load_model)

    checkpointDict = loadModelCheckpointDict(opt.load_model, device)

    if 'model_type' not in checkpointDict:
        print('Error: wrong model type')
        exit(1)

    opt.model_type = checkpointDict['model_type']

    _, _, trainableModel, ensemble_types = loadEnsembleFromCheckpoint(checkpointDict, None, None)

    trainableModel = trainableModel.to(device)

    optimizer = torch.optim.AdamW(trainableModel.parameters(), lr=lr, weight_decay=opt.weight_decay )
    lr_scheduler = None #torch.optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.5)

    criterion = nn.CrossEntropyLoss()

elif opt.load_checkpoint is not None:

    print('Loading model from previously saved checkpoint', opt.load_checkpoint)

    checkpointDict = loadModelCheckpointDict(opt.load_checkpoint, device)
    if 'model_type' not in checkpointDict:
        print('Error: wrong model type')
        exit(1)

    opt.model_type = checkpointDict['model_type']

    startEpoch, _, trainableModel, ensemble_types = loadEnsembleFromCheckpoint(checkpointDict, None, None)
    
    params = [p for p in trainableModel.parameters() if p.requires_grad]

    optimizer = torch.optim.AdamW(params, lr=lr, weight_decay=opt.weight_decay )
    lr_scheduler = None #torch.optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.5)

    criterion = nn.CrossEntropyLoss()

    trainableModel.to(device)
    loadEnsembleFromCheckpoint(checkpointDict, optimizer, lr_scheduler, criterion)

    print('Starting at epoch', startEpoch)

else:
    model_paths = [
        'checkpoints_comp/run2020813_115211/model_epoch_007.pth',
        #'checkpoints_comp/run2020813_115211/model_epoch_027.pth',
        #'checkpoints_comp/run2020814_123646/model_epoch_098.pth',
        #'checkpoints_comp/run2020815_94843/model_epoch_046.pth',
        'checkpoints_comp/run2020815_94843/model_epoch_020.pth'
    ]

    device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')

    ensembleModel, largest_model_type, ensemble_types = loadEnsembleModel(model_paths, device)
    
    opt.model_type = largest_model_type

    optimizer = torch.optim.AdamW(ensembleModel.parameters(), lr=opt.lr, weight_decay=opt.weight_decay )
    lr_scheduler = None #torch.optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.5)

    criterion = nn.CrossEntropyLoss()

    trainableModel = ensembleModel

opt.image_size = EfficientNet.get_image_size('efficientnet-b{}'.format(opt.model_type - 1))

train_dataset = ImageFolder(os.path.join(opt.dataset_root, 'train'), getTrainingTransforms(opt.image_size))

val_dataset = ImageFolder(os.path.join(opt.dataset_root, 'val'), getValidationTransforms(opt.image_size))

trainLoader = torch.utils.data.DataLoader(train_dataset, 
                                            batch_size=opt.batchSize, 
                                            shuffle=True, 
                                            num_workers=opt.threads)

valLoader = torch.utils.data.DataLoader(val_dataset, 
                                            batch_size=opt.valBatchSize, 
                                            shuffle=True, 
                                            num_workers=opt.threads)

class_names = train_dataset.classes

dataloaders = { 
    'train': trainLoader,
    'val': valLoader
}

# Get a batch of training data
inputs, classes = next(iter(dataloaders['train']))

# Make a grid from batch
out = torchvision.utils.make_grid(list(inputs))

imshow(out, title=[class_names[x] for x in classes])

print('Run Parameters:')
print('\tImage Size:', opt.image_size)
print('\tTrain Batch Size:', opt.batchSize)
print('\tVal Batch Size:', opt.valBatchSize)
print('\tEpochs:', opt.epochs)
print('\tLR:', opt.lr)
print('\tDevice:', device)
print('\tThreads:', opt.threads)
print('Starting at epoch', startEpoch)
print()

best_model = train_ensemble(trainableModel, dataloaders, optimizer, lr_scheduler, criterion, device, ensemble_types, num_epochs, checkpoint_dir=checkpoint_dir, start_epoch=startEpoch)

saveEnsembleCheckpoint(num_epochs + startEpoch - 1, 
                    0, 
                    trainableModel, 
                    ensemble_types,
                    optimizer, 
                    lr_scheduler, 
                    os.path.join(checkpoint_dir, 'model_final.pth'),
                    criterion,
                    opt.model_type)

saveFileName = 'final_model_isic{}.pth'.format(getTimestamp())
saveFilePath = os.path.join(opt.model_save_dir, saveFileName)
torch.save(best_model, saveFilePath)
print('Model file saved to {}'.format(saveFilePath))