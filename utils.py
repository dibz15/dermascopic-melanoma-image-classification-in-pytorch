import cv2 
import matplotlib.pyplot as plt
import torch
import torchvision
import numpy as np
from torch.utils.data import SubsetRandomSampler, WeightedRandomSampler
from torch import nn
from torchvision import transforms
from PIL import Image
from datetime import datetime

from torchvision.models.detection.faster_rcnn import FasterRCNN
from torchvision.models.detection import MaskRCNN
from torchvision.models.detection.rpn import AnchorGenerator

from efficientnet_pytorch import EfficientNet

from collections import OrderedDict
from tqdm import tqdm
import os, yaml

mean_sums = [0.485, 0.456, 0.406]
std_sums = [0.229, 0.224, 0.225]

def getTestTransforms():
    return transforms.Compose([
        # transforms.Resize(512),
        #transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean_sums, std_sums)
    ])

def getMobileNetV2Classifier(num_classes=2, model_state=None, pretrained=True):
    model = torchvision.models.mobilenet_v2(pretrained=pretrained)
    fc = nn.Sequential(
        nn.Linear(1280, 400),
        nn.ReLU(),
        nn.Dropout(0.4),
        nn.Linear(400,num_classes),
        nn.LogSoftmax(dim=1)
    )
    model.classifier = fc

    if model_state is not None:
        model.load_state_dict(model_state, strict=False)

    return model

def getMobileNetV3Classifier(num_classes=2, large=True, model_state=None, pretrained=True):
    if large:
        model = torchvision.models.mobilenet_v3_large(pretrained=pretrained)
    else:
        model = torchvision.models.mobilenet_v3_small(pretrained=pretrained)

    fc = nn.Sequential(
        nn.Linear(960, 400),
        nn.ReLU(),
        nn.Dropout(0.2),
        nn.Linear(400,num_classes),
        nn.LogSoftmax(dim=1)
    )
    model.classifier = fc
    
    if model_state is not None:
        model.load_state_dict(model_state, strict=False)

    return model

def getEfficientNetClassifier(num_classes=2, size='b0', dropout=0.4, model_state=None, pretrained=True):

    if pretrained:
        model = EfficientNet.from_pretrained('efficientnet-{}'.format(size), num_classes=num_classes)
    else:
        model = EfficientNet.from_name('efficientnet-{}'.format(size), num_classes=num_classes)
    
    model._dropout = nn.Dropout(dropout)

    if model_state is not None:
        model.load_state_dict(model_state)

    return model

# def getEfficientNetV2Classifier(num_classes=2, dropout=0.4):
#     model = torch.hub.load('pytorch/vision', 'efficientnet_v2_s', pretrained=True)

#     print(model)

def renameStateDictParameters(old_dict, key_transform):
    new_dict = OrderedDict()
    for key, value in old_dict.items():
        new_key = key_transform(key)
        new_dict[new_key] = value

    return new_dict

def getMobileNetV2Backbone(frozen=True, backbone_state=None):
    backbone_model = torchvision.models.mobilenet_v2(pretrained=True)

    if frozen:
        for param in backbone_model.parameters():
            param.requires_grad = False

        backbone_model.classifier = nn.Sequential(
            nn.Dropout(p=0.2, inplace=False),
            nn.Linear(1280, 1000, bias=True)
        )

    backbone = backbone_model.features
    #MobileNet_V2 has 1280 output channels
    backbone.out_channels = 1280

    if backbone_state is not None:
        backbone.load_state_dict(backbone_state, strict=False)

    return backbone

#Pulled from https://pytorch.org/tutorials/intermediate/torchvision_tutorial.html
#situation 2 (replacing backbone)
def getModifiedFasterRCNN(min_size=224, imageScale=0.5, frozenBackbone=False, backbone_state=None, model_state=None):
    if backbone_state is None and model_state is not None:
        def k_trans(key):
            if key.startswith('backbone.'):
                return key.replace('backbone.', '')
            else:
                return key
        #Backbone state is stored within full model_state, no need to load it separately if we can rename the params
        backbone_state = renameStateDictParameters(model_state, k_trans)

    backbone = getMobileNetV2Backbone(frozen=frozenBackbone, backbone_state=backbone_state)

    #RPN for 5 x 3 anchors per spatial location (5 different sizes, 3 different spatial ratios)
    sizes = [16, 32, 64, 128, 256]
    sizes = [int(size * imageScale) for size in sizes]

    anchor_generator = AnchorGenerator(sizes=(sizes,), 
                                                aspect_ratios=((0.65, 1.0, 1.5),))

    #Region of Interest cropping, and crop size after rescaling
    roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'], 
                                                    output_size=7,
                                                    sampling_ratio=2)

    model = FasterRCNN(backbone, num_classes=2, rpn_anchor_generator=anchor_generator,
                        box_roi_pool=roi_pooler, min_size=min_size)

    if model_state is not None:
        model.load_state_dict(model_state)

    return model

#Pulled from https://pytorch.org/tutorials/intermediate/torchvision_tutorial.html
#situation 2 (replacing backbone)
def getModifiedMaskRCNN(min_size=224, imageScale=0.5, frozenBackbone=False, backbone_state=None, model_state=None):
    if backbone_state is None and model_state is not None:
        def k_trans(key):
            if key.startswith('backbone.'):
                return key.replace('backbone.', '')
            else:
                return key
        #Backbone state is stored within full model_state, no need to load it separately if we can rename the params
        backbone_state = renameStateDictParameters(model_state, k_trans)

    backbone = getMobileNetV2Backbone(frozen=frozenBackbone, backbone_state=backbone_state)

    #RPN for 5 x 3 anchors per spatial location (5 different sizes, 3 different spatial ratios)
    sizes = [32, 64, 128, 256, 512]
    #sizes = [int(size * imageScale) for size in sizes]

    anchor_generator = AnchorGenerator(sizes=(sizes,), 
                                                aspect_ratios=((1.0, 1.2, 1.4),))

    #Region of Interest cropping, and crop size after rescaling
    roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'], 
                                                    output_size=7,
                                                    sampling_ratio=2)

    mask_roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'], 
                                                output_size=14,
                                                sampling_ratio=2)
                                                
    model = MaskRCNN(backbone, num_classes=2, rpn_anchor_generator=anchor_generator,
                        box_roi_pool=roi_pooler, mask_roi_pool=mask_roi_pooler, min_size=min_size)

    if model_state is not None:
        model.load_state_dict(model_state)

    return model

def showImage(imageTensor, boxes=[]):
    #img = transforms.ToPILImage()(imageTensor)
    img = imageTensor.mul(255).permute(1, 2, 0).byte().numpy()
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    for i in range(len(boxes)):
        start_coords = (boxes[i][0], boxes[i][1])
        end_coords = (boxes[i][2], boxes[i][3])
        cv2.rectangle(img, start_coords, end_coords, color=(0, 255, 0), thickness=3)

        # plt.figure(figsize=(20, 30))
    plt.imshow(img)
    plt.xticks([])
    plt.yticks([])
    plt.show()

def collate_fn(batch):
    return tuple(zip(*batch))

def saveModelState(model, path):
    torch.save(model.state_dict(), path)

def loadModelState(model, path, device='cpu'):
    model.load_state_dict(torch.load(path, map_location=device))

def saveModelCheckpoint(epoch, loss, model, optimizer, scheduler, path, criterion=None, model_type=None):
    torch.save({
        'epoch': epoch,
        'model_state': model.state_dict(),
        'optimizer_state': optimizer.state_dict(),
        'scheduler_state': scheduler.state_dict() if scheduler is not None else None,
        'criterion_state': criterion.state_dict() if criterion is not None else None,
        'loss': loss,
        'model_type': model_type
    }, path)

def loadModelCheckpointDict(path, device='cpu'):
    return torch.load(path, map_location=device)

def loadModelFromCheckpoint(checkpointDict, optimizer, scheduler, criterion=None, dropout=0.4):
    checkpoint = checkpointDict

    if 'model_type' in checkpoint and checkpoint['model_type'] is not None:
        if checkpoint['model_type'] == 0:
            model = getMobileNetV2Classifier(num_classes=2, model_state=checkpoint['model_state'])
        elif checkpoint['model_type'] >= 1 and checkpoint['model_type'] < 8:
            model = getEfficientNetClassifier(num_classes=2, size='b{}'.format(checkpoint['model_type']-1), dropout=dropout, model_state=checkpoint['model_state'])
        elif checkpoint['model_type'] >= 8:
            model = getMobileNetV3Classifier(num_classes=2, model_state=checkpoint['model_state'])

    if optimizer is not None and checkpoint['optimizer_state'] is not None:
        optimizer.load_state_dict(checkpoint['optimizer_state'])

    if scheduler is not None and checkpoint['scheduler_state'] is not None:
        scheduler.load_state_dict(checkpoint['scheduler_state'])

    if criterion is not None and checkpoint['criterion_state'] is not None:
        criterion.load_state_dict(checkpoint['criterion_state'])

    return checkpoint['epoch'], checkpoint['loss'], model

def getSubSampler(percentage, indices):
    np.random.shuffle(indices)

    numIndices = int(len(indices) * percentage)

    return SubsetRandomSampler( indices[0:numIndices] )


def get_class_distribution(dataset_obj):
    cached_weights = {}
    if os.path.isfile('class_weights.cache'):
        try:
            print(f'Reading class weights from cache for root {dataset_obj.root}.')
            with open('class_weights.cache', 'r') as f:
                cached_weights = yaml.safe_load(f)
                if dataset_obj.root in cached_weights:
                    return cached_weights[dataset_obj.root]
        except:
            print('Error reading class weights from cache. Recalculating...')
            pass

    count_dict = {k:0 for k,v in dataset_obj.class_to_idx.items()}
    idx2class = {v: k for k, v in dataset_obj.class_to_idx.items()}

    for element in tqdm(dataset_obj):
        y_lbl = element[1]
        y_lbl = idx2class[y_lbl]
        count_dict[y_lbl] += 1
            
    cached_weights[dataset_obj.root] = count_dict
    with open('class_weights.cache', 'w') as wf:
        print(f'Writing weights {count_dict} to cache for root {dataset_obj.root}')
        yaml.dump(cached_weights, wf)

    return count_dict

def getCrossEntropyLossWeights(class_count_dict):
    class_count = [i for i in class_count_dict.values()]
    size_largest_class = max(class_count)
    return torch.tensor([size_largest_class / i for i in class_count])

def getWeightedSampler(dataset):
    target_list = torch.tensor(dataset.targets)
    class_count = [i for i in get_class_distribution(dataset).values()]
    class_weights = 1. / torch.tensor(class_count, dtype=torch.float)
    class_weights_all = class_weights[target_list]

    return WeightedRandomSampler(weights=class_weights_all, num_samples=len(class_weights_all), replacement=True)

def showPredictions(img, resultList):
    img.convert('RGB')
    img = np.array(img)
    # img = img[:, :, ::-1].copy()

    boxes = resultList['boxes']
    scores = resultList['scores']
    labels = resultList['labels']

    for i in range(len(boxes)):
        start_coords = (boxes[i][0], boxes[i][1])
        end_coords = (boxes[i][2], boxes[i][3])
        cv2.rectangle(img, start_coords, end_coords, color=(0, 255, 0), thickness=2)
        cv2.putText(img, 
                    '{:1.3f}'.format(scores[i]),
                    (boxes[i][0], boxes[i][1]),
                    cv2.FONT_HERSHEY_SIMPLEX, 
                    fontScale=1, color=(0, 255, 0), thickness=2)

        print('Box:', boxes[i], '\t\tScore:', scores[i], '\tClass:', labels[i])
        # plt.figure(figsize=(20, 30))

    plt.imshow(img)
    plt.xticks([])
    plt.yticks([])
    plt.show()


def showPredictionsNumpy(img, resultList):
    img.convert('RGB')
    img = np.array(img)
    # img = img[:, :, ::-1].copy()

    for i in range(len(resultList)):
        currBox = resultList[i]
        start_coords = (int(round(currBox[0])), int(round(currBox[1])))
        end_coords = (int(round(currBox[2])), int(round(currBox[3])))
        cv2.rectangle(img, start_coords, end_coords, color=(0, 255, 0), thickness=2)
        cv2.putText(img, 
                    '{:1.3f}'.format(currBox[4]),
                    (int(round(currBox[0])), int(round(currBox[1]))),
                    cv2.FONT_HERSHEY_SIMPLEX, 
                    fontScale=0.5, color=(0, 255, 0), thickness=2)

        print('Box:', currBox[0:4], '\t\tScore:', currBox[4], '\tClass:', currBox[5])
        # plt.figure(figsize=(20, 30))

    plt.imshow(img)
    plt.xticks([])
    plt.yticks([])
    plt.show()

def getModelPredictions(images, model, device, scoreThreshold):
    model = model.to(device)
    model.eval()
    modelInput = [getTestTransforms()(image).to(device) for image in images]

    pred = model(modelInput)

    imageResults = []
    for prediction in pred:
        res = {}
        res['boxes'] = np.array([[box[0], box[1], box[2], box[3]] for box in prediction['boxes'].detach().cpu().numpy()])
        res['scores'] = np.array([score for score in prediction['scores'].detach().cpu().numpy()])
        res['labels'] = np.array([cl for cl in prediction['labels'].detach().cpu().numpy()])

        threshIndices = np.where(res['scores'] > scoreThreshold)

        res['boxes'] = res['boxes'][threshIndices]
        res['scores'] = res['scores'][threshIndices]
        res['labels'] = res['labels'][threshIndices]

        imageResults.append(res)

    return imageResults

#BBox format of [x1, y1, x2, y2]
def calculateBBoxIoU(bbox1, bbox2):    
    if (bbox1[0] >= bbox1[2]) or (bbox1[1] >= bbox1[3]):
        raise AssertionError('BBOX1 is not valid.')
    if (bbox2[0] >= bbox2[2]) or (bbox2[1] >= bbox2[3]):   
        raise AssertionError('BBOX2 is not valid.')

    # If bottom-right corner of bbox 1 is less than or above the top-left 
    # corner of bbox 2.
    if (bbox1[2] < bbox2[0]) or (bbox1[3] < bbox2[1]):
        return 0.0 

    # if top-left corner of bbox 1 is greater than or below bottom-right
    # corner of bbox 2
    if (bbox1[0] > bbox2[2]) or (bbox1[1] > bbox2[3]):
        return 0.0

    bbox1_area = (bbox1[2] - bbox1[0]) * (bbox1[3] - bbox1[1])
    bbox2_area = (bbox2[2] - bbox2[0]) * (bbox2[3] - bbox2[1])

    #Calculate intersection, which is a box made up of max(top left corners) and
    # min(bottom right corners)
    int_tl = [ max(bbox1[0], bbox2[0]), max(bbox1[1], bbox2[1]) ]
    int_br = [ min(bbox1[2], bbox2[2]), min(bbox1[3], bbox2[3]) ]

    int_area = ( int_br[0] - int_tl[0] ) * ( int_br[1] - int_tl[1] )

    #Calculate union area
    union_area = (bbox1_area + bbox2_area) - int_area

    #Result is ratio of intersection/union

    return int_area / union_area

def calculateBBoxIoU_fast(bbox1, otherBBoxes):
    
    x1 = otherBBoxes[:,0]
    y1 = otherBBoxes[:,1]
    x2 = otherBBoxes[:,2]
    y2 = otherBBoxes[:,3]

    #Compute bounding box areas and sort the boxes by bottom-right y-coordinate of 
    # the bounding box
    area1 = (bbox1[2] - bbox1[0] + 1) * (bbox1[3] - bbox1[1] + 1)
    areaOther = (x2 - x1 + 1) * (y2 - y1 + 1)

    # find the largest (x, y) coordinates for the start of
    # the bounding box and the smallest (x, y) coordinates
    # for the end of the bounding box
    xx1 = np.maximum(bbox1[0], x1)
    yy1 = np.maximum(bbox1[1], y1)
    xx2 = np.minimum(bbox1[2], x2)
    yy2 = np.minimum(bbox1[3], y2)

    # compute the width and height of the bounding box
    w = np.maximum(0, xx2 - xx1 + 1)
    h = np.maximum(0, yy2 - yy1 + 1)

    int_area = w * h
    union_area = (area1 + areaOther) - int_area

    # compute the ratio of overlap
    iou = int_area / union_area

    return iou


def sortPredictionsByClass(predictions, num_classes):
    classDict = {}

    combinedArray = np.zeros((0, 6))
    for pred in predictions:
        boxes = pred['boxes'].detach().cpu().numpy()
        scores = pred['scores'].detach().cpu().numpy()
        labels = pred['labels'].detach().cpu().numpy()
        
        combinedArray = np.row_stack([combinedArray, np.column_stack([boxes, scores, labels])])

    for c in range(1, num_classes + 1):
        classIndices = np.where(combinedArray[:, 5] == c)
        classDict[c] = combinedArray[classIndices]

    return classDict

def sortTargetsByClass(predictions, num_classes):
    classDict = {}

    combinedArray = np.zeros((0, 5))
    for pred in predictions:
        boxes = pred['boxes'].detach().cpu().numpy()
        labels = pred['labels'].detach().cpu().numpy()

        combinedArray = np.row_stack([combinedArray, np.column_stack([boxes, labels])])

    for c in range(1, num_classes + 1):
        classIndices = np.where(combinedArray[:, 4] == c)
        classDict[c] = combinedArray[classIndices]

    return classDict

def cleanupMasks(preds):
    combinedArray = []
    for pred in preds:
        masks = pred['masks'].detach().cpu().numpy()
        
        cleanedMasks = []
        for mask in masks:
            firstChannel = mask[0]
            cleanedMasks.append(firstChannel)

        combinedArray.append(np.array(cleanedMasks))

    return np.array(combinedArray)

def nonMaximumSuppression_slow(classPredictions, confThresh = 0.2, iouThresh=0.4):
    if len(classPredictions) == 0:
        return []

    # Eliminate predictions with a score less than confThresh
    predIndices = np.where(classPredictions[:, 4] > confThresh)

    allIndexSet = set(predIndices[0])
    suppressedIndices = set()

    for predictionIdx in allIndexSet:
        if predictionIdx in suppressedIndices:
            continue
        for otherIdx in allIndexSet ^ suppressedIndices:
            if otherIdx == predictionIdx:
                continue
            iou = calculateBBoxIoU(classPredictions[predictionIdx, 0:4], classPredictions[otherIdx, 0:4])
            print('Prediction Idx:', predictionIdx)
            print('Other Idx:', otherIdx)
            print(iou)
            if iou > iouThresh:
                print('Suppressing', otherIdx)
                suppressedIndices.add(otherIdx)
            print()

    return classPredictions[list(allIndexSet ^ suppressedIndices)]

    # From https://www.pyimagesearch.com/2015/02/16/faster-non-maximum-suppression-python/
def nonMaximumSuppression_pick(boxes, confThresh = 0.5, iouThresh=0.4):
    """
    Expects classPredictions as a numpy array that is [N x [box[0:4], confidence, class]]

    """

    #If no boxes, don't run algorithm
    if len(boxes) == 0:
        return []

    # Make sure it's a float array 
    if boxes.dtype.kind == 'i':
        boxes.astype('float')

    #Picked indices list
    pick = []

    #Filter indices by confidence score threshold (My added)
    boxes = boxes[np.where(boxes[:, 4] > confThresh)]

    x1 = boxes[:,0]
    y1 = boxes[:,1]
    x2 = boxes[:,2]
    y2 = boxes[:,3]

    #Compute bounding box areas and sort the boxes by bottom-right y-coordinate of 
    # the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)

    while len(idxs) > 0:
        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)

        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])

        # compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)

        # compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]

        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last],
            np.where(overlap > iouThresh)[0])))

    return pick

# From https://www.pyimagesearch.com/2015/02/16/faster-non-maximum-suppression-python/
def nonMaximumSuppression_fast(boxes, confThresh = 0.5, iouThresh=0.4):
    """
    Expects classPredictions as a numpy array that is [N x [box[0:4], confidence, class]]

    """
    pick = nonMaximumSuppression_pick(boxes, confThresh, iouThresh)

    return boxes[pick]



def getTimestamp():
    t = datetime.now()
    return "{:04}{:02}{:02}_{:02}{:02}{:02}".format(t.year, t.month, t.day, t.hour, t.minute, t.second)


def resizeImage(image, imageScale):
    originalImgWidth, originalImgHeight = image.size

    image = cv2.resize(np.array(image), (int(originalImgWidth * imageScale), int(originalImgHeight * imageScale)))
    image = Image.fromarray(image)

    return image

def runModelTest(image, model, imageScale, device, confThreshold=0.5, iouThreshold=0.4):
    finalImage = resizeImage(image, imageScale)

    #results = getModelPredictions([finalImage], model, device, threshold)

    model = model.to(device)
    model.eval()
    modelInput = [getTestTransforms()(finalImage).to(device)]
    pred = model(modelInput)
    sortedDict = sortPredictionsByClass(pred, 1)

    boxes = np.ndarray((0, 6))
    for k in sortedDict.keys():
        boxes = np.row_stack([boxes, nonMaximumSuppression_fast(sortedDict[k], confThresh=confThreshold, iouThresh=iouThreshold)])

    showPredictionsNumpy(finalImage, boxes)

def scaleBoxes(boxes, scale=1.0, center=None):
    """
    Scales the given boxes from the center of the boxes or from the given center, to the scale.

    This works by transforming the coordinate system to center at (0, 0), upscaling, then transforming back.

    Ex. A box (2, 2, 4, 4) and scale = 2 will scale the box to (1, 1, 5, 5).
    """
    for i, box in enumerate(boxes):
        if center is None:
            oCenter = [(box[2] - box[0]) / 2 + box[0], (box[3] - box[1]) / 2 + box[1]]
        else:
            oCenter = center
        zeroedCoords = [box[0] - oCenter[0], box[1] - oCenter[1], box[2] - oCenter[0], box[3] - oCenter[1]]
        scaledCoords = [coord * scale for coord in zeroedCoords]
        
        for j in range(4):
            boxes[i][j] = oCenter[j % 2] + scaledCoords[j]

    return boxes

def cvPutBoxes(img, boxes):
    for i in range(len(boxes)):
        currBox = boxes[i]
        start_coords = (int(round(currBox[0])), int(round(currBox[1])))
        end_coords = (int(round(currBox[2])), int(round(currBox[3])))
        cv2.rectangle(img, start_coords, end_coords, color=(0, 255, 0), thickness=2)
        cv2.putText(img, 
                    '{:1.3f}'.format(currBox[4]),
                    (int(round(currBox[0])), int(round(currBox[1]))),
                    cv2.FONT_HERSHEY_SIMPLEX, 
                    fontScale=0.5, color=(0, 255, 0), thickness=2)

        print('Box:', currBox[0:4], '\t\tScore:', currBox[4], '\tClass:', currBox[5])

def visualizeConfusionMatrix(conf_matrix, labels=[['False', 'True']] * 2):
    # imports
    import seaborn as sebrn

    # Using Seaborn heatmap to create the plot
    fx = sebrn.heatmap(conf_matrix, annot=True, cmap='cool')

    # labels the title and x, y axis of plot
    fx.set_title('Confusion matrix\n\n')
    fx.set_xlabel('Predicted Values')
    fx.set_ylabel('Actual Values')

    # labels the boxes
    fx.xaxis.set_ticklabels(labels[0])
    fx.yaxis.set_ticklabels(labels[1])

    plt.show()    


def createConfusionMatrix(tp, tn, fp, fn):
    return [[tp, fn], [fp, tn]]